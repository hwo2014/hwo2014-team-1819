require 'json'
require 'socket'
require 'forwardable'
require 'pry'

Dir[File.expand_path(File.join(File.dirname(__FILE__), 'lib', '**', '*.rb'))].each { |f| require f }

def host_game(driver_class, players = 1)
  @threads << Thread.new do
    race = Games::Tournament.new(driver_class, Configurations::TeamServer.new)
    race.start(@track, players, false)
  end
end

def join_game(driver_class, players)
  @threads << Thread.new do
    race = Games::Tournament.new(driver_class, Configurations::TeamServer.new)
    race.start(@track, players, true)
  end
end

@threads = []
# @track = 'usa'
# @track = 'germany'
@track = 'keimola'
drivers = [Drivers::BestLiner, Drivers::Hunt]

host_game(drivers.first, drivers.count)
sleep(2)
drivers[1..drivers.count].each { |driver| join_game(driver, drivers.count) }
@threads.each { |thread| thread.join }



