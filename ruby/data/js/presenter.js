var angleData = {};
var speedData = {};
var throttleData = {};
var angleSpeedData = {};
var angleAccelerationData = {};
var accelerationData = {};
var sectionData = {};

var arrayLength = metricsData.length;
for (var i = 0; i < arrayLength; i++) {
    var metric = metricsData[i];
    var tick = format_tick(metric['tick']);
    speedData[tick] = metric['momentum']['speed'];
    sectionData[tick] = metric['position']['section'];
    throttleData[tick] = metric['momentum']['throttle'];
    angleData[tick] = metric['position']['angle'];
    angleSpeedData[tick] = metric['momentum']['angular_speed'] || 0;
    accelerationData[tick] = metric['momentum']['acceleration'] || 0;
    angleAccelerationData[tick] = metric['momentum']['angular_acceleration'] || 0;
}

function format_tick(tick) {
//    ("000000" + tick).substr(tick.length, tick.length + 6);
    return 10000 + tick
}


$(document).ready(function () {
//    new Chartkick.LineChart("chart-1", {"2013-02-10 00:00:00 -0800": 11, "2013-02-11 00:00:00 -0800": 6});
    new Chartkick.LineChart("chart-1", angleData, {"library": {"title": {text: "Angle"}, "chart": {"zoomType": "x"}}});
    new Chartkick.LineChart("chart-2", angleSpeedData, {"library": {"title": {text: "Angle Speed"}, "chart": {"zoomType": "x"}}});
    new Chartkick.LineChart("chart-3", speedData, {"library": {"title": {text: "Speed"}, "chart": {"zoomType": "x"}}});
    new Chartkick.LineChart("chart-4", [
        {name: 'angular accel', yAxis: 0, data: angleAccelerationData},
        {name: 'throttle', yAxis: 1, data: throttleData},
        {name: 'angle', yAxis: 2, data: angleData},
        {name: 'section', yAxis: 2, data: sectionData}
    ], {"library": {"title": {text: "Combined"},
                    "chart": {"zoomType": "x"},
    yAxis: [{title: 'Speed'}, {title: 'Throttle'}, {title: 'Angle'}, {title: 'Section'}]}});
});