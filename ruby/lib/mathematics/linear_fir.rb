require 'matrix'

module Mathematics
  class LinearFit
    def initialize(array_xx, array_y)
      @x = Matrix.columns(array_xx.map { |el| el.clone.unshift(1) })
      @x = @x.transpose
      @y = Matrix.columns([array_y])
    end

    def theta
      return @theta if @theta
      @theta = (@x.transpose * @x).inverse * @x.transpose * @y
    rescue ExceptionForMatrix::ErrNotRegular
      puts 'ExceptionForMatrix::ErrNotRegular'
      @theta = [0,0]
    end

    def calculate(array_x)
      result = Matrix.rows([array_x.clone.unshift(1)]) * theta
      result[0,0]
    end
  end
end