module Mathematics
  class DiffSolver
    attr_reader :physics, :calculator
    attr_accessor :throttle, :advance_throttle

    attr_reader :lin_acceleration, :lin_speed, :x
    attr_reader :ang_acceleration, :ang_speed, :a

    def initialize(physics)
      @physics = physics
      @calculator = physics.calculator

      @throttle = 1
      @advance_throttle = 1
    end

    def starting_at(metric)
      @x = 0
      @lin_speed = metric.momentum.speed
      @a = metric.position.angle
      @ang_speed = metric.momentum.angular_speed
    end


    def calculate_tick(radius = 0, angle = 0)
      set_accelerations(radius, angle)
      set_speeds
      set_coords
    end

    def get_solver_after_distance(distance)
      tick = 0
      while x < distance && tick < 300
        tick += 1
        calculate_tick
      end
      @x = x - distance
      self
    end

    def will_crash_on_distance(distance, radius = 0, angle = 0)
      radius = Math.sqrt(radius)
      tick = 0
      while x < distance && tick < 300
        tick += 1
        calculate_tick(radius, angle)
        return true if a.abs > physics.danger_angle
      end
      false
    end

    def going_to_crash_on_brake?(distance, radius = 0, angle = 0)
      return true if a.abs > physics.danger_angle
      radius = Math.sqrt(radius)
      @throttle = 0
      while x < distance && still_going?
        calculate_tick(radius, angle)
        return true if a.abs > physics.danger_angle
      end
      crop_distance(distance)
      false
    end

    def reached_distance_end?(distance)
      x < distance
    end

    def reach_max_angle(distance, radius, angle)
      radius = Math.sqrt(radius)
      while x < distance && still_going?
        calculate_tick(radius, angle)
        break if a.abs > physics.warning_angle
      end
      true
    end

    def advance(throttle, radius = 0, angle = 0)
      @throttle = throttle
      calculate_tick(radius, angle)
      true
    end

    def safe_conditions?
      a.abs < physics.danger_angle
    end

    def can_advance?(distance)
      x < distance && still_going?
    end

    def crop_distance(distance)
      @x = x - distance
    end

    def still_going?
      lin_speed > 3
    end


    def max_angle_safe?(distance, radius, angle)
      radius = Math.sqrt(radius)
      @throttle = 1
      while x < distance
        calculate_tick(radius, angle)
        return false if a.abs > physics.critical_angle
        break if a.abs > physics.warning_angle
      end
      @throttle = calculator.steady_throttle(@lin_speed)
      while x < distance
        calculate_tick(radius, angle)
        return false if a.abs > physics.danger_angle
      end
      5.times do
        calculate_tick
        return false if a.abs > physics.critical_angle
      end
      true
    end

    def set_accelerations(radius, angle)
      @lin_acceleration = calculator.linear_acceleration(@throttle, @lin_speed)
      @ang_acceleration = calculator.centripetal_force(@lin_speed, radius, angle) + calculator.drift_force(@a, @ang_speed, @lin_speed)
    end

    def set_speeds
      @lin_speed += @lin_acceleration
      @ang_speed += @ang_acceleration
    end

    def set_coords
      @x += @lin_speed
      @a += @ang_speed
    end


    def critical_angle
      physics.danger_angle
    end


  end
end