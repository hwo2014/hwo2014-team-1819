require 'benchmark'
module Games
  class Tournament
    attr_reader :driver_class, :config, :communicator, :messages_parser
    attr_reader :car, :track, :participants, :opponents, :driver, :physics
    attr_reader :race_initialized

    def initialize(driver_class, config)
      @driver_class, @config = driver_class, config
      @participants, @opponents = [], []
      @communicator = Games::Communicator.new(config)
      @messages_parser = Games::MessagesParser.new
      @physics = Physics::Coefficients.new
    end

    def start(track, cars = 1, join = false)
      init_game(track, cars, join)
      communicate
    end

    def join
      @communicator.init('join', {})
      communicate
    end

    def communicate
      communicator.communicate do |type, data|
        result = handle_communication(type, data)
        if type =='gameStart' || (type === 'carPositions' && data['gameTick'])
          result
        else
          nil
        end
      end
    end

    def handle_communication(type, data)
      handle_method = :"handle_#{underscore(type)}"
      if respond_to?(handle_method)
        send(handle_method, data)
      else
        handle_unknown(type, data)
      end
    rescue => e
      puts "Exception #{e} trying to skip"
      puts e.backtrace
      ['throttle', 0.4]
    end

    def handle_create_race(data)
      puts "Joined to race #{data['data']['trackName']} with #{data['data']['carCount'] - 1} opponents"
      default_response
    end

    def handle_join_race(data)
      puts 'Race joined'
    end

    def handle_join(data)
      puts 'Race joined'
    end

    def handle_your_car(data)
      @car = messages_parser.parse_your_car(data, self)
      default_response
    end

    def handle_game_init(data)
      initialize_race(data)
      default_response
    end

    def handle_car_positions(data)
      tick = data['gameTick']
      messages_parser.update_cars_positions(data, participants)
      drive_track(tick)
    end

    def handle_lap_finished(data)
      car = messages_parser.car_for_message(participants, data)
      if car
        car.lap_finished(messages_parser.parse_lap_finished(data))
      else
        puts 'Batman car finished lap %)'
      end
      default_response
    end

    def handle_finish(data)
      handle_car_message(data, :finished)
    end

    def handle_dnf(data)
      handle_car_message(data, :dnf)
    end

    def handle_game_start(data)
      @race_started = true
      reset_race_data
      drive_track(data['gameTick'])
    end

    def handle_crash(data)
      handle_car_message(data, :crashed)
    end

    def handle_spawn(data)
      handle_car_message(data, :spawned)
    end

    def handle_turbo_available(data)
      #puts 'Received turbo. Get ready for action'
      turbo_parameters = messages_parser.parse_turbo_data(data)
      participants.each { |car| car.turbo_available(*turbo_parameters) }
      default_response
    end

    def handle_turbo_start(data)
      handle_car_message(data, :turbo_start)
    end

    def handle_turbo_end(data)
      handle_car_message(data, :turbo_end)
    end

    def handle_car_message(data, method)
      car = messages_parser.car_for_message(participants, data)
      if car
        car.send(method)
      end
      default_response
    end

    def handle_game_end(data)
      puts 'Race finished'
    end

    def handle_tournament_end(data)
      puts 'Season finished'
    end

    def race_started?(tick)
      @race_started && tick
    end

    def drive_track(tick)
      return default_response unless race_started?(tick)
      response = driver.drive_track(tick)
      car.set_throttle(response[1]) if response[0] == 'throttle'
      response
    end

    def default_response
      ['ping']
    end

    def handle_unknown(type, data)
      puts "WARNING: UNKNOWN Message #{type} with: #{data}"
      default_response
    end

    def init_game(track, cars, join)
      init_message = join ? 'joinRace' : 'createRace'
      @communicator.init(init_message, {trackName: track, carCount: cars})
    end

    def underscore(camel_cased_word)
      word = camel_cased_word.to_s.dup
      word.gsub!(/([A-Z]+)([A-Z][a-z])/, '\1_\2')
      word.gsub!(/([a-z\d])([A-Z])/, '\1_\2')
      word.tr!("-", "_")
      word.downcase!
      word
    end

    def initialize_race(data)
      puts "Initializing race #{messages_parser.parse_race_type(data)}"
      return if @race_initialized
      @track = messages_parser.parse_track(data)
      @track.prepare_for_race(self)
      @participants = messages_parser.parse_participants(data, @car, self)
      @opponents = participants.select { |participant| car != participant }
      @driver = driver_class.new(self, track, car)
      @race_initialized = true
      # messages_parser.parse_training_json(self, 'data/best_metrics/usa1.json')
      puts 'All data parsed'
    end

    def reset_race_data
      puts 'Reseting race metrics'
      participants.each do |car|
        car.prepare_for_race
      end
    end
  end
end
