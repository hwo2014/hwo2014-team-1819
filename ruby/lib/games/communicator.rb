module Games
  class Communicator
    attr_reader :config, :socket, :communication_initialized

    def initialize(config)
      @config = config
      @socket = TCPSocket.open(config.server_host, config.server_port)
    end

    def init(message, data)
      init_data = config.join_data.merge(data)
      puts "Initializing game #{message} with options #{init_data}"
      send_message(message, init_data)
      @communication_initialized = true
    end

    def communicate(&block)
      raise 'not initialized' unless communication_initialized

      while json = socket.gets do
        message = JSON.parse(json)
        response = yield(message['msgType'], message)
        send_message(*response) if response
      end
    end

    private
    def send_message(type, data = {})
      socket.puts JSON.generate({:msgType => type, :data => data})
    end

  end
end
