class Games::MessagesParser
  def parse_your_car(message, race)
    car_data = message['data']
    Cars::Owned.new(car_data['color'], car_data['name'], race)
  end

  def parse_track(message)
    Tracks::Parser.from_data(message)
  end

  def parse_participants(message, owned_car, race)
    cars_data = message['data']['race']['cars']
    cars_data.map do |car_data|
      parse_car(car_data, owned_car, race)
    end
  end

  def update_cars_positions(message, cars)
    tick = get_tick(message)
    message['data'].each do |position_data|
      position = parse_position_data(position_data['piecePosition'], position_data['angle'])
      car = cars.find { |car| car.has_color?(get_car_color(position_data)) }
      car.set_position(position, tick) if car
    end
  end

  def get_car_color(car_data)
    car_data['id']['color']
  end

  def parse_car(car_data, owned_car, race)
    car_color = get_car_color(car_data)
    car_dimension = car_data['dimensions']
    car = owned_car.has_color?(car_color) ? owned_car : Cars::Opponent.new(car_color, car_data['id']['name'], race)
    car.set_dimensions(car_dimension['length'], car_dimension['width'], car_dimension['guideFlagPosition'])
    car
  end

  def parse_position(message, car = nil)
    car_data = get_car_data(message, car)
    position = parse_position_data(car_data['piecePosition'], car_data['angle'])
    Metrics::Drive.new(Metrics::Momentum.empty, position, get_tick(message))
  end

  def get_tick(message)
    message['gameTick']
  end

  def parse_position_data(piece_data, angle)
    Metrics::Position.new(piece_data['lane']['endLaneIndex'], piece_data['pieceIndex'], piece_data['inPieceDistance'], angle, piece_data['lap'], piece_data['lane']['startLaneIndex'])
  end

  def get_car_data(message, car)
    message['data'][0]
  end

  def parse_training_json(race, file)
    whole_metrics = JSON.parse(IO.read(file))
    car = parse_car(whole_metrics.first['data']['race']['cars'].first, Cars::Owned.new('red', 'dummy', race), race)
    whole_metrics.each do |message|
      next unless message['msgType'] == 'fullCarPositions'
      car.set_position(parse_position_data(message['data'].first['piecePosition'], message['data'].first['angleOffset']), get_tick(message))
    end
    # messages_parser.parse_training_json(self, 'data/best_metrics/germany1.json')
    car.save_metrics(race.config)
    raise 'abort'
  end

  def parse_race_type(data)
    race_data = data['data']['race']['raceSession']
    if race_data['durationMs']
      {qualification: {duration: race_data['durationMs']}}
    else
      {race: {laps: race_data['laps']}}
    end
  end

  def car_for_message(participants, data)
    return nil unless data.is_a?(Hash)
    data.each_pair do |key, value|
      car = car_for_message(participants, value)
      return car if car
      return participants.find { |car| car.has_color?(value) } if key == 'color'
    end
  end

  def parse_lap_finished(data)
    lap_data = data['data']
    {ranking: lap_data['ranking']['overall'],
     fastest: lap_data['ranking']['fastestLap'],
     laps: lap_data['raceTime']['laps'],
     lap_time: lap_data['lapTime']['millis']}
  end

  def parse_turbo_data(data)
    turbo_data = data['data']
    [turbo_data['turboFactor'], turbo_data['turboDurationTicks']]
  end
end