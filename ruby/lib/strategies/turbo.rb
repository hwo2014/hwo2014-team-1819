module Strategies
  module Turbo
    def get_throttle(car, next_part, distance_to_part)
      if car.has_turbo? && should_use_turbo?(car, next_part, distance_to_part)
        car.turbo_start
        puts 'Using turbo!!!. Weeeeeee!!!'
        ['turbo', 'Weeeeeee!!!']
      else
        super
      end
    end

    def should_use_turbo?(car, next_part, distance_to_part)
      return false unless car.laps >= 1
      current_section = current_part.track.section_for(car.position)
      current_section.turbo_support
    end

    def join_straight(next_part)
      return 0 if next_part.curve?
      next_part.length + join_straight(next_part.strategy.following_part)
    end
  end
end