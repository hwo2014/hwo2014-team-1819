class Strategies::Builder
  attr_reader :race
  def initialize(race)
    @race = race
  end

  def assign_strategy(part, previous_curve, next_curve, next_part)
    if part.curve?
      assign_curve_strategy(part, previous_curve, next_curve, next_part)
    else
      assign_straight_strategy(part, previous_curve, next_curve, next_part)
    end
  end

  def assign_straight_strategy(part, previous_curve, next_curve, next_part)
    strategy = build_strategy(Strategies::Straight, part, next_curve, previous_curve, next_part)
    part.assign_strategy(strategy)
  end

  def build_strategy(strategy, part, next_curve, previous_curve, next_part)
    strategy = strategy.new(part, race.physics)
    strategy.set_neighbor_curves(previous_curve, next_curve, next_part)
    strategy
  end

  def assign_curve_strategy(part, previous_curve, next_curve, next_part)
    strategy = build_strategy(curve_strategy(part), part, next_curve, previous_curve, next_part)
    part.assign_strategy(strategy)
  end

  def curve_strategy(part)
    Strategies::Curve
  end
end