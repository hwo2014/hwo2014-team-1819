module Strategies
  class Base
    attr_reader :current_part, :physics, :throttle_control
    attr_reader :previous_curve, :next_curve, :following_part

    def initialize(current_part, physics)
      @current_part, @physics = current_part, physics
      @throttle_control = Physics::StraightThrottleControl.new(physics)
      create_analyzers
    end

    def set_neighbor_curves(previous_curve, next_curve, following_part)
      @previous_curve, @next_curve, @following_part = previous_curve, next_curve, following_part
      current_part.update_entering_speed(default_entering_speed)
    end

    def get_throttle(car, next_parts, distance_to_part)
      ['throttle', throttle_factor(car, next_parts, distance_to_part)]
    end

    def part_crashed(by_car)
    end

    def max_safe_throttle(car, next_parts, distance_to_part)
      return 1 unless going_to_crash?(car, next_parts, distance_to_part)
      0
    end

    def find_safe_throttle(car, next_part, distance_to_part, lower, upper, iterations)
      return lower unless iterations > 0
      middle = (lower + upper) / 2.0
      if going_to_crash?(car, next_part, distance_to_part, middle)
        upper = middle
      else
        lower = middle
      end
      find_safe_throttle(car, next_part, distance_to_part, lower, upper, iterations - 1)
    end

    def going_to_crash?(car, next_parts, distance_to_part, advance_throttle = 1)
      solver = Mathematics::DiffSolver.new(physics)
      solver.starting_at(car)
      crash_danger_on_break?(solver, distance_to_part, next_parts, advance_throttle, 5)
    end

    def crash_danger_on_break?(solver, distance_to_part, next_parts, advance_throttle = 1, advance_ticks = 0)
      radius_sqrt = Math.sqrt(current_radius)
      while solver.can_advance?(distance_to_part) && solver.safe_conditions?
        advance_throttle, advance_ticks = check_advance_strategy(advance_throttle, advance_ticks)
        solver.advance(advance_throttle, radius_sqrt, current_angle)
      end
      return true unless solver.safe_conditions?
      return false unless solver.still_going?
      return false if next_parts.empty?
      solver.crop_distance(distance_to_part)
      next_part = next_parts.first
      next_part.strategy.crash_danger_on_break?(solver, next_part.length, next_parts.slice(1, next_parts.size), advance_throttle, advance_ticks)
    end

    def check_advance_strategy(throttle, ticks)
      return [0, 0] unless ticks > 0
      [throttle, ticks - 1]
    end

    def advance_with_throttle(solver, advance_throttle, advance_ticks)
      solver.advance_throttle
    end

    def current_radius
      0
    end

    def current_angle
      0
    end

    def create_analyzers
      @all_analyzers = []
    end

    def analytics_event(car, event)
      @all_analyzers.map { |analyzer| analyzer.send(event, car) }
    end

    def print_analytic_data
      puts @all_analyzers.map { |analyzer| analyzer.describe }.join("\n")
    end

    def default_entering_speed
      10
    end
  end
end