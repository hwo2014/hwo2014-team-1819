require "#{File.dirname(__FILE__)}/turbo"

class Strategies::Straight < Strategies::Base
  include Strategies::Turbo

  def throttle_factor(car, next_parts, distance_to_part)
    max_safe_throttle(car, next_parts, distance_to_part)
  end

  def part_crashed(car)
    previous_curve.strategy.part_crashed(car)
  end

end