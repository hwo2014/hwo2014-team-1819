class Strategies::Curve < Strategies::Base

  def create_analyzers
    @curve_analyzer = Physics::CurveMetricsAnalyzer.new(physics)
    @angle_analytic = Strategies::Analyzers::MaxAngle.new
    @min_speed_analytic = Strategies::Analyzers::MinSpeed.new
    @max_speed_analytic = Strategies::Analyzers::MaxSpeed.new
    @boundaries_speed_analytic = Strategies::Analyzers::BoundariesSpeed.new
    @all_analyzers = [@angle_analytic, @min_speed_analytic, @max_speed_analytic, @boundaries_speed_analytic]
  end

  def throttle_factor(car, next_parts, distance_to_part)
    analytics_event(car, :update_metrics)
    max_safe_throttle(car, next_parts, distance_to_part)
  end

  def current_radius
    @current_radius ||= current_part.radius
  end

  def current_angle
    @current_angle ||= current_part.angle
  end

  def part_crashed(car)
    @current_radius = current_radius * 0.8
  end

  def default_entering_speed
    [physics.calculator.max_curve_speed(current_part.radius), max_entering_speed].min
  end

  def max_entering_speed
    throttle_control.speed_delta_for(current_part.length + current_part.distance_to_part(next_curve)) + next_curve.entering_speed
  end

end