class Strategies::Analyzers::DataAnalyzer < Strategies::Analyzers::Abstract
  def part_enter(for_car)
    reset_analytic(for_car)
    analytic(for_car)[:data] = []
  end

  def part_exit(for_car)
    update_metrics(for_car)
    analyze_data(for_car)
  end

  def update_metrics(car)
    datum = {
        angle: car.position.angle,
        angular_speed: car.momentum.angular_speed,
        angular_acceleration: car.momentum.angular_acceleration,
        speed: car.momentum.speed,
    }
    analytic(car)[:data] << datum
  end

  def get_value(car)
    analytic(car)[:max_angle] || 0
  end

  def describe
    "Theta: #{describe_data}"
  end

  def analyze_data(car)
    radius = car.current_part.radius.to_f
    part_angle = car.current_part.angle.to_f
    y, xx = [], []
    data = analytic(car)[:data]
    data.shift #ignore first datum
    data.each_with_index do |datum, i|
      sign = part_angle > 0 ? 1 : -1
      x1 = sign * datum[:speed] #/ (radius)
      x2 = datum[:angle]
      x4 = datum[:angular_speed]
      y << datum[:angular_acceleration]


      xx << [x1, x2, x4]
    end
    @solver = Mathematics::LinearFit.new(xx, y)
    p @solver.theta.to_a
    p "part_angle: #{part_angle}, radius: #{radius}"
  end

end