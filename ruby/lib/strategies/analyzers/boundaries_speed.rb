class Strategies::Analyzers::BoundariesSpeed < Strategies::Analyzers::Abstract
  def part_enter(car)
    analytic(car)[:enter_speed] = car.current_speed
  end

  def part_exit(car)
    analytic(car)[:exit_speed] = car.current_speed
  end

  def update_metrics(car)
    part_enter(car) unless analytic(car)[:enter_speed]
  end

  def enter_speed(car)
    analytic(car)[:enter_speed] || 1
  end

  def exit_speed(car)
    analytic(car)[:exit_speed] || 1
  end

  def describe
    "Max speed: #{describe_data}"
  end

end
