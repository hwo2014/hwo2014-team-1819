module Strategies
  module Analyzers
    class Abstract
      attr_reader :analytic_data

      def initialize
        @analytic_data = {}
      end

      def reset_analytic(by_car)
        analytic_data[by_car] = {}
      end

      def analytic(car)
        analytic_data[car] ||= {}
      end

      def part_enter(for_car)
      end

      def part_exit(for_car)
      end

      def update_metrics(car)
      end

      def describe_data
        analytic_data.each_pair.map { |car, analytic| "Driver: #{car.driver_name} - #{analytic}"}.join("\n\t\t")
      end
    end
  end
end