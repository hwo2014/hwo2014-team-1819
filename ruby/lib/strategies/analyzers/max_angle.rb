class Strategies::Analyzers::MaxAngle < Strategies::Analyzers::Abstract
  def part_enter(for_car)
    reset_analytic(for_car)
  end

  def part_exit(for_car)
    update_metrics(for_car)
  end

  def update_metrics(car)
    max_angle = get_value(car)
    current_angle = car.position.angle
    analytic(car)[:max_angle] = current_angle if current_angle.abs > max_angle
  end

  def get_value(car)
    analytic(car)[:max_angle] || 0
  end

  def describe
    "Max angle: #{describe_data}"
  end

end
