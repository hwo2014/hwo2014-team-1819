class Strategies::Analyzers::MinSpeed < Strategies::Analyzers::Abstract
  def part_enter(for_car)
    reset_analytic(for_car)
  end

  def part_exit(for_car)
    update_metrics(for_car)
  end

  def update_metrics(car)
    min_speed = get_value(car)
    current_speed = car.current_speed
    analytic(car)[:min_speed] = current_speed if current_speed < min_speed
  end

  def get_value(car)
    analytic(car)[:min_speed] || 10
  end

  def describe
    "Min speed: #{describe_data}"
  end

end
