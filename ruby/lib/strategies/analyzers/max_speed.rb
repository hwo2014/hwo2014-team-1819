class Strategies::Analyzers::MaxSpeed < Strategies::Analyzers::Abstract
  def part_enter(for_car)
    reset_analytic(for_car)
  end

  def part_exit(for_car)
    update_metrics(for_car)
  end

  def update_metrics(car)
    max_speed = get_value(car)
    current_speed = car.current_speed
    analytic(car)[:max_speed] = current_speed if current_speed > max_speed
  end

  def get_value(car)
    analytic(car)[:max_speed] || 0
  end

  def describe
    "Max speed: #{describe_data}"
  end

end
