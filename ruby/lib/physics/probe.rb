class Physics::Probe
  attr_reader :metrics, :speed_array

  def initialize(metrics)
    @metrics = metrics
    @speed_array = Array.new(400, 0)
  end

  def update_momentum
    current_metric, previous_metric = metrics.last, metrics.previous
    update_speed(current_metric, previous_metric)
    update_acceleration(current_metric, previous_metric)
    update_angular_speed(current_metric, previous_metric)
    update_angular_acceleration(current_metric, previous_metric)
    check_switch_lane_spikes(current_metric, previous_metric)
  end

  def aw_calculated(physics, metric)
    physics.damping_a * metric.momentum.angular_speed + physics.damping_b * metric.momentum.speed * metric.position.angle
  end

  def print_centripetial_force(physics, section)
    return unless section.curve?
    current_metric, previous_metric = metrics.last, metrics.previous
    aw_calculated = aw_calculated(physics, previous_metric)
    aw_real = current_metric.momentum.angular_acceleration
    diff = (aw_real - aw_calculated).abs
    puts "#{section.describe}\t#{previous_metric.momentum.speed}\t#{aw_real}\t#{aw_calculated}\t#{diff * section.radius}"
  end

  def update_linear_momentum
    current_metric, previous_metric = metrics.last, metrics.previous
    update_speed(current_metric, previous_metric)
    update_acceleration(current_metric, previous_metric)
  end

  def update_linear_coefficients(physics)
    return if physics.linear_defined?
    return unless (metrics = linear_valid_metrics)
    v = metrics.map { |metric| metric.momentum.speed }
    friction = (v[2] - 2 * v[1] + v[0]) / (v[0] - v[1])
    power = (friction - 1)* v[1] + v[2]
    puts "Physics Linear coefficients defined: power: #{power} - friction: #{friction}"
    return puts "Cant define coefficients: power: #{power} - friction: #{friction}" if friction < 0 || power < 0
    return unless power.abs.between?(0.1, 0.3)
    return unless friction.abs.between?(0.01, 0.04)
    physics.set_linear(power, friction)
  end

  def update_angular_coefficients(physics, section)
    update_angular_force_coeficients(physics, section)
    update_angular_damping_coeficients(physics, section)
  end

  def update_angular_force_coeficients(physics, section)
    return if physics.centripetal_force_defined?
    return unless physics.damping_defined?
    return unless section.curve?
    return unless (metrics = angular_force_valid_metrics)
    v = metrics.map { |metric| metric.momentum.speed }
    r = section.radius
    cf = metrics.first(3).each_with_index.map { |metric, index| metrics[index + 1].momentum.angular_acceleration - physics.calculator.damping_force(metric) }
    a_coef = (cf[1] * v[2] - cf[2] * v[1]) / (v[1] * v[2] * (v[1] - v[2]))
    a_coef_check = (cf[0] * v[1] - cf[1] * v[0]) / (v[0] * v[1] * (v[0] - v[1]))
    return if cf[1].abs < 0.1e-5 || cf[2].abs < 0.1e-5
    return unless a_coef_check.between?(a_coef - 0.0001, a_coef + 0.0001)
    b_coef = a_coef * v[1] - cf[1] / v[1]
    a_coef = a_coef * Math.sqrt(r)
    return unless a_coef.abs.between?(0.3, 0.9)
    return unless b_coef.abs.between?(0.15, 0.45)
    puts "Physics curve force coefficients defined: #{r}\t#{a_coef}\t#{b_coef}"
    physics.set_force(a_coef.abs, b_coef.abs)
  end

  def update_angular_damping_coeficients(physics, section)
    return if physics.damping_defined?
    return if section.curve?
    return unless (metrics = angular_damping_valid_metrics)
    v = metrics.map { |metric| metric.momentum.speed }
    a = metrics.map { |metric| metric.position.angle }
    vw = metrics.map { |metric| metric.momentum.angular_speed }
    aw = metrics.map { |metric| metric.momentum.angular_acceleration }
    p1 = aw[3] - aw[2] * vw[2] / vw[1]
    p2 = v[2] * a[2] - (v[1] * a[1] * vw[2])/(vw[1])
    b_coef = p1 / p2
    a_coef = (aw[2] - b_coef * v[1] * a[1]) / (vw[1])
    return unless a_coef.abs.between?(0.05, 0.15)
    return unless b_coef.abs.between?(0.00062, 0.00186)
    puts "Physics Angular damping coefficients defined: a_coef: #{a_coef} - b_coef: #{b_coef}"
    physics.set_damping(a_coef, b_coef)
  end

  def average_speed
    speed_array.inject(0) { |sum, speed| sum + speed } / speed_array.size.to_f
  end

  private

  def linear_valid_metrics
    return if metrics.size < 3
    return unless metrics[-3].momentum.same_throttle?(metrics[-2].momentum)
    return unless metrics.previous.momentum.throttle > 0.99
    return unless metrics.previous.momentum.moving?
    metrics.last(3)
  end

  def angular_force_valid_metrics
    return if metrics.size < 5
    return unless metrics[-4].position.section == metrics.last.position.section
    metrics.last(4)
  end

  def angular_damping_valid_metrics
    return if metrics.size < 5
    return unless metrics[-4].position.section == metrics.last.position.section
    return unless metrics[-4].position.angle.abs > 0
    metrics.last(4)
  end

  def update_speed(current_metric, previous_metric)
    speed = current_metric.distance.absolute - previous_metric.distance.absolute
    update_speed_array(speed, current_metric.tick)
    current_metric.momentum.speed = speed
  end

  def update_speed_array(speed, tick)
    speed_array[tick % speed_array.size] = speed
  end

  def update_acceleration(current_metric, previous_metric)
    acceleration = current_metric.momentum.speed - previous_metric.momentum.speed
    current_metric.momentum.acceleration = acceleration
  end

  def update_angular_speed(current_metric, previous_metric)
    speed = current_metric.position.angle - previous_metric.position.angle
    current_metric.momentum.angular_speed = speed
  end

  def update_angular_acceleration(current_metric, previous_metric)
    acceleration = current_metric.momentum.angular_speed - previous_metric.momentum.angular_speed
    current_metric.momentum.angular_acceleration = acceleration
  end

  def check_switch_lane_spikes(current_metric, previous_metric)
    return if current_metric.position.switching_lane?
    return unless previous_metric.position.switching_lane?
    metrics.last.momentum.speed = previous_metric.momentum.speed
    metrics.last.momentum.acceleration = previous_metric.momentum.acceleration
  end


end