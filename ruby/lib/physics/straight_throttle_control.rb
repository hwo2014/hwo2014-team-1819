class Physics::StraightThrottleControl
  attr_reader :physics

  COMPARING_DELTA = 0.1

  def initialize(physics)
    @physics = physics
  end

  def get_throttle(current_velocity, target_velocity, target_distance)
    return 0 if target_distance_very_close?(current_velocity, target_distance)
    return 1.0 if target_velocity > current_velocity
    return 1.0 if break_distance_far_ahead?(current_velocity, target_velocity, target_distance)
    0
  end

  def target_distance_very_close?(current_velocity, target_distance)
    target_distance < current_velocity
  end

  def break_distance_far_ahead?(current_velocity, target_velocity, target_distance)
    break_distance(current_velocity, target_velocity) + target_velocity < target_distance
  end

  def break_distance(current_velocity, target_velocity)
    (current_velocity - target_velocity) / physics.friction
  end

  def speed_delta_for(distance)
    distance * physics.friction
  end

  def turbo_activity_time(target_speed, current_speed, distance, power_factor)
    default_time_till_breaking = time_till_breaking(target_speed, current_speed, distance, 1)
    turbo_time_till_breaking = time_till_breaking(target_speed, current_speed, distance, power_factor)
    default_time_till_breaking - turbo_time_till_breaking
  end

  def time_till_breaking(target_speed, current_speed, distance, power_factor = 1)
    (target_speed - current_speed + physics.friction * distance) / (power_factor * physics.power)
  end

  def steadily_throttle(target_velocity)
    [physics.friction * target_velocity / physics.power, 1].min
  end

  def close_values?(current_velocity, target_velocity)
    return false unless target_velocity + COMPARING_DELTA > current_velocity
    return false unless target_velocity - COMPARING_DELTA < current_velocity
    0
  end
end