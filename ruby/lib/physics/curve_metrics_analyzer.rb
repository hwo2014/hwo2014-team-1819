class Physics::CurveMetricsAnalyzer
  attr_reader :physics
  def initialize(physics)
    @physics = physics
  end

  def crash_danger?(car, curve_end_time, default_danger_time)
    angle, angular_speed, angular_acceleration = expand_metrics(car)
    danger_time = [curve_end_time, default_danger_time].min
    return true if max_angle_critical?(angle, angular_speed, angular_acceleration, danger_time)
    return true if angle_peak_outside_limit?(angle, angular_speed, angular_acceleration, danger_time)
    return true if angle_velocity_too_high?(angle, angular_speed, angular_acceleration, curve_end_time, default_danger_time)
    false
  end

  def max_angle_critical?(angle, angular_speed, angular_acceleration, time)
    result = angle + angular_speed * time + angular_acceleration * time ** 2 / 2
    result.abs > physics.danger_angle
  end

  def angle_peak_outside_limit?(angle, angular_speed, angular_acceleration, danger_time)
    return false if angular_acceleration == 0 || angular_speed * angular_acceleration > 0
    danger_time = [(angular_speed / angular_acceleration).abs, danger_time].min
    max_angle_critical?(angle, angular_speed, angular_acceleration, danger_time)
  end

  def angle_velocity_too_high?(angle, angular_speed, angular_acceleration, curve_end_time, danger_time)
    return false unless curve_end_time < danger_time
    result = angle + angular_speed * curve_end_time + angular_acceleration * curve_end_time ** 2 / 2
    curve_end_speed = angular_speed + angular_acceleration * curve_end_time
    time_outside_curve = danger_time - curve_end_time
    return true if angle_peak_outside_limit?(result, curve_end_speed, angle > 0 ? -0.3 : 0.3, time_outside_curve)
    max_angle_critical?(result, curve_end_speed, angle > 0 ? -0.3 : 0.3, time_outside_curve)
  end


  def angular_acceleration_peak?(curve_angle, metrics)
    current_angle, angular_speed, angular_acceleration = expand_metrics(metrics)
    if curve_angle > 0
      if current_angle > 0
        if angular_speed > 0
          if angular_acceleration < 0
            true
          else
            false
          end
        else
          if angular_acceleration < 0
            true
          else
            false
          end
        end
      else
        false
      end
    else
      if current_angle < 0
        if angular_speed < 0
          if angular_acceleration > 0
            true
          else
            false
          end
        else
          if angular_acceleration > 0
            true
          else
            false
          end
        end
      else
        false
      end
    end
  end

  def no_drift?(metrics)
    current_angle, angular_speed, angular_acceleration = expand_metrics(metrics)
    current_angle.abs < 5 && angular_speed.abs < 1 && angular_acceleration.abs < 0.5
  end

  def expand_metrics(metrics)
    [metrics.position.angle, metrics.momentum.angular_speed, metrics.momentum.angular_acceleration]
  end
end