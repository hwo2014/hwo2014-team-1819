module Physics
  class Coefficients
    attr_reader :power, :friction, :turbo_factor,
                :force_a, :force_b, :damping_a, :damping_b,
                :linear_defined, :damping_defined, :force_defined,
                :calculator

    def initialize
      @power = 0.2
      @turbo_factor = 1
      @friction = 0.015
      @linear_defined = false
      @damping_a = -0.1
      @damping_b = -0.00125
      @damping_defined = false
      @force_a = 0.8
      @force_b = 0.3
      @force_defined = false
      @calculator = Physics::Calculator.new(self)
    end

    def set_linear(power, friction)
      return if linear_defined?
      @power, @friction = power, friction
      @linear_defined = true
    end

    def set_damping(a, b)
      return if damping_defined?
      @damping_a, @damping_b = a, b
      @damping_defined = true
    end

    def set_force(a, b)
      return if centripetal_force_defined?
      @force_a, @force_b = a, b
      @force_defined = true
    end

    def enable_turbo(factor)
      @turbo_factor = factor
    end

    def disable_turbo
      @turbo_factor = 1
    end

    def linear_defined?
      @linear_defined
    end

    def damping_defined?
      @damping_defined
    end

    def centripetal_force_defined?
      @force_defined
    end

    def critical_angle
      58
    end

    def danger_angle
      56
    end

    def warning_angle
      54
    end

    def critical_angle_radians
      1.02974426
    end

    def curve_coefficient1
      0.245 * critical_angle_radians
    end

    def curve_coefficient2
      0.35
    end

    def power_ratio
      power / friction
    end

    def power
      @power * @turbo_factor
    end
  end
end
