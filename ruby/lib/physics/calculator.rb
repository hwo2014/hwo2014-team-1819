module Physics
  class Calculator
    attr_reader :coefficients

    def initialize(coefficients)
      @coefficients = coefficients
    end

    def damping_force(metric)
      turning_friction(metric) + drift_friction(metric)
    end

    def angular_force(metric, radius_sqrt, angle)
      centripetal_force(metric, radius_sqrt, angle) + damping_force(metric)
    end

    def centripetal_force(speed, radius_sqrt, angle)
      return 0 unless radius_sqrt > 0.1
      clear_force = coefficients.force_a * speed * speed / radius_sqrt
      cutoff_force = coefficients.force_b * speed
      force = clear_force - cutoff_force
      return 0 if force < 0
      similar_sign(angle) * force
    end

    def drift_force(angle, angular_speed, speed)
      coefficients.damping_b * angle * speed + coefficients.damping_a * angular_speed
    end

    def drift_friction(metric)
      angle = metric.position.angle
      speed = metric.momentum.speed
      coefficients.damping_b * angle * speed
    end

    def steady_throttle(speed)
      [coefficients.friction * speed / coefficients.power, 1].min
    end

    def turning_friction(metric)
      angular_speed = metric.momentum.angular_speed
      coefficients.damping_a * angular_speed
    end

    def linear_acceleration(throttle, speed)
      coefficients.power * throttle - speed * coefficients.friction
    end

    def max_curve_speed(radius)
      Math.sqrt(radius) * (coefficients.force_b + coefficients.danger_angle * coefficients.damping_b.abs) / coefficients.force_a
    end

    def similar_sign(value)
      value >= 0 ? 1 : -1
    end

    def opposite_sign(value)
      value >= 0 ? -1 : 1
    end
  end
end