class Physics::CurveSpeedCalculator
  include Math
  attr_reader :coefficients

  def initialize(coefficients)
    @coefficients = coefficients
  end

  def minimal_curve_point(radius, angle)
    curve_distance = curve_distance(radius, angle)
    curve_max_speed = curve_max_speed(radius)
    curve_brake_distance = curve_brake_distance(curve_distance)
    curve_min_speed = curve_min_speed(curve_brake_distance, curve_max_speed)
    [curve_brake_distance, curve_min_speed]
  end

  def curve_min_speed(distance, v_last)
    v_last - coefficients.friction * distance
  end

  def curve_brake_distance(curve_distance)
    curve_distance * 0.35
  end

  def curve_distance(radius, angle)
    (2 * PI * radius * angle.abs / 360)
  end

  def curve_max_speed(radius)
    sqrt((coefficients.curve_coefficient1 + coefficients.curve_coefficient2) * radius)
  end

end