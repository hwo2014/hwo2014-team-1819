class Tracks::Parts::Curve < Tracks::Parts::Base
  attr_reader :radius, :angle

  def set_curve_parameters(radius, angle)
    @radius, @angle = radius, angle
    @entering_speed = Math.sqrt(0.245 + 0.35 * @radius)
  end

  def curve?
    true
  end

  def radius
    if first_section.switchable?
      @radius - 10
    else
      @radius
    end
  end

end