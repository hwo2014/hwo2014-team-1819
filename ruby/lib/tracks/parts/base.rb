module Tracks::Parts
  class Base
    attr_reader :lane, :included_sections, :length
    attr_reader :entering_speed
    attr_reader :strategy, :track

    def initialize(lane, included_sections, length)
      @lane, @included_sections, @length = lane, included_sections, length
    end

    def get_throttle(car, next_part, distance_to_part)
      strategy.get_throttle(car, next_part, distance_to_part)
    end

    def assign_strategy(strategy)
      @strategy = strategy
    end

    def assign_track(track)
      @track = track
    end

    def update_entering_speed(speed)
      @entering_speed = speed
    end

    def describe
      "lane#{first_section.lane_number}-sections[#{first_section.section_number}..#{last_section.section_number}]"
    end

    def first_section
      included_sections.first
    end

    def last_section
      included_sections.last
    end

    def distance_to_end(position)
      total_distance = included_sections.select { |section| section.section_number >= position.section }.inject(0) {|sum, s| sum + s.length}
      total_distance - position.distance
    end

    def distance_from_start(position)
      total_distance = included_sections.select { |section| section.section_number < position.section }.inject(0) {|sum, s| sum + s.length}
      total_distance + position.distance
    end

    def curve?
      false
    end

    def distance_to_part(next_part)
      return 0 if next_part == self
      if next_part.first_section.section_number > last_section.section_number
        next_part.first_section.lane_distance_from_start - last_section.lane_distance_from_start
      else
        last_section.lane_distance_to_finish + next_part.first_section.lane_distance_from_start
      end
    end
  end

  def self.assign_to_sections(sections, track)
    start = sections.first
    part = construct_part(sections, start, track)
    sections.each { |section| section.set_track_part(part) }
  end

  def self.construct_part(sections, start, track)
    part_length = sum_sections(sections, :length)
    if start.curve?
      part = Tracks::Parts::Curve.new(start.lane_number, sections, part_length)
      part.set_curve_parameters(start.radius, sum_sections(sections, :angle))
      part
    else
      part = Tracks::Parts::Straight.new(start.lane_number, sections, part_length)
    end
    part.assign_track(track)
    part
  end

  def self.sum_sections(sections, method)
    sections.inject(0) { |sum, s| sum + s.send(method) }
  end
end