class Tracks::Parts::Null < Tracks::Parts::Base
  def initialize
    super(0, [], 10)
  end

  def get_throttle(car, next_part, distance_to_part)
    0.5
  end

  def entering_speed
    100
  end

  def angle
    0
  end

  def absolute_distance_from_start
    10000000
  end
end