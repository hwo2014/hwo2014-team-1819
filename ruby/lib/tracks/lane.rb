class Tracks::Lane
  attr_reader :track, :number, :offset, :sections, :analytic

  def initialize(track, number, offset)
    @track, @number, @offset = track, number, offset
    @sections = []
    @analytic = {}
  end

  def length
    analytic[:length]
  end

  def add_straight(length)
    sections << Tracks::Section::Straight.new(number, sections.count, length)
  end

  def add_curve(radius, angle)
    additional_radius = angle > 0 ? -offset : offset
    sections << Tracks::Section::Curve.new(number, sections.count, radius + additional_radius, angle)
  end

  def set_switch(section_number, neighbor_lanes)
    sections[section_number].set_switch(neighbor_lanes)
  end

  def distance_to_curve(section, distance)
    sections[section].distance_to_curve(distance)
  end
end