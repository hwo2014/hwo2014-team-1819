module Tracks
  class Analyzer
    attr_reader :track

    def initialize(track)
      @track = track
      @speed_calculator = Physics::CurveSpeedCalculator.new(Physics::Coefficients.new)
    end

    def prepare_for_race(race)
      prepare_lane_distances
      prepare_best_distances
      prepare_track_connections
      prepare_speed_limits
      prepare_track_parts
      prepare_track_strategies(race)
      prepare_track_turbos
    end

    def prepare_track_connections
      lanes.each_with_index do |lane, lane_number|
        lane.sections.each_with_index do |section, section_number|
          section.set_next_section(track.next_section(lane_number, section_number))
        end
      end
    end

    def prepare_track_parts
      lanes.each_with_index do |lane, lane_number|
        create_track_part(lane_number, 0)
      end
    end

    def prepare_track_turbos
      lanes.each_with_index do |lane, lane_number|
        straights = lane.sections.map do |section|
          [distance_to_curve(lane_number, section.section_number), section]
        end
        straights = straights.sort_by { |distance, section| -distance }
        assign_turbo(straights.first[1])
        straights.select { |distance, section| distance > 400 }.each { |distance, section| assign_turbo(section) }
      end
    end

    def assign_turbo(section)
      section.turbo_support = true
    end

    def prepare_track_strategies(race)
      strategies_builder = Strategies::Builder.new(race)
      lanes.each_with_index do |lane, lane_number|
        lane.sections.reverse.each do |section|
          current_part = section.track_part
          previous_curve_part = preceding_curve_part(current_part, lane_number, section.section_number)
          next_curve_part = following_curve_part(current_part, lane_number, section.section_number)
          next_part = following_part(current_part, lane_number, section.section_number)
          strategies_builder.assign_strategy(current_part, previous_curve_part, next_curve_part, next_part)
        end
      end
    end

    def following_curve_part(current_part, lane_number, section_number)
      section = track.sections_starting_from(lane_number, section_number).find { |section| section.curve? && section.track_part != current_part }
      section && section.track_part || track.sections_starting_from(lane_number, section_number).last.track_part
    end

    def following_part(current_part, lane_number, section_number)
      section = track.sections_starting_from(lane_number, section_number).find { |section| section.track_part != current_part }
      section && section.track_part || track.sections_starting_from(lane_number, section_number).last.track_part
    end

    def preceding_curve_part(current_part, lane_number, section_number)
      section = track.sections_preceding_to(lane_number, section_number).find { |section| section.curve? && section.track_part != current_part }
      section && section.track_part || track.sections_preceding_to(lane_number, section_number).last.track_part
    end

    def create_track_part(lane, section_start)
      start_section = track.get_section(lane, section_start)
      sections = track.sections_starting_from(lane, section_start).take_while { |section| start_section == section || (section.similar_to?(start_section) && !section.switchable?) }
      sections = [start_section] if start_section.switchable?
      end_section = sections.last
      Tracks::Parts.assign_to_sections(sections, track)
      return if start_section.section_number > end_section.section_number || end_section.finish? #we set part for sections going through finish line
      create_track_part(lane, end_section.next_section.section_number)
    end

    def prepare_speed_limits
      lanes.each_with_index do |lane, lane_number|
        calculate_speed_limit(lane_number, 0)
      end
    end

    def calculate_speed_limit(lane_number, start_section)
      curve = track.lanes[lane_number].sections.find { |section| section.curve? && section.section_number >= start_section }
      return unless curve
      curves = track.sections_starting_from(lane_number, curve.section_number).take_while { |section| section.curve? }
      radius = curve.radius
      curves = curves.take_while { |section| section.radius == radius }
      angle = curves.inject(0) { |sum, section| sum + section.angle }
      speed_limit = @speed_calculator.minimal_curve_point(radius, angle)
      sections_distance = 0
      curves.each do |curve|
        sections_distance += curve.length
        next if sections_distance < speed_limit[0]
        current_distance = speed_limit[0] + curve.length - sections_distance
        curve.set_analytic(:speed_limit, [current_distance, speed_limit[1]])
      end
      calculate_speed_limit(lane_number, curves.last.section_number + 1)
    end

    def prepare_lane_distances
      lanes.each_with_index do |lane, lane_number|
        lane.sections.each_with_index do |section, section_number|
          section.set_analytic(:lane_distance_from_start, lane_distance_from_start(lane_number, section_number))
          section.set_analytic(:lane_distance_to_finish, lane_distance_to_finish(lane_number, section_number))
          section.set_analytic(:lane_distance_to_curve, distance_to_curve(lane_number, section_number))
        end
      end
    end

    def prepare_best_distances
      (track.sections_count - 1).downto(0) do |section_number|
        lanes.each_with_index do |lane, lane_number|
          section(lane, section_number).set_analytic(:shortest_distance_to_finish, short_distance_to_finish(lane_number, section_number))
          section(lane, section_number).set_analytic(:optimal_switches, optimal_switches(lane_number, section_number))
        end
      end
    end

    def lane_distance_to_finish(lane_number, section_number)
      sum_length(track.sections_till_finish(lane_number, section_number))
    end

    def lane_distance_from_start(lane_number, section_number)
      sum_length(track.sections_from_start(lane_number, section_number).first(section_number))
    end

    def short_distance_to_finish(lane_number, section_number)
      current_section = track.get_section(lane_number, section_number)
      next_sections = track.possible_sections_from(lane_number, section_number)
      if next_sections.any? { |section| section.start? }
        current_section.length
      else
        next_sections.map(&:shortest_distance_to_finish).min + current_section.length
      end
    end

    def optimal_switches(lane_number, section_number)
      current_section = track.get_section(lane_number, section_number)
      return nil unless current_section.switchable?
      next_sections = track.possible_sections_from(lane_number, section_number)
      next_sections.sort_by { |section| section.shortest_distance_to_finish }.map(&:lane_number)
    end

    def distance_to_curve(lane_number, section_number)
      straight_sections = track.sections_starting_from(lane_number, section_number).slice_before { |section| section.curve? }
      straight_sections = straight_sections.first
      return 0 if straight_sections.first.curve?
      sum_length(straight_sections)
    end

    def distance_between(end_position, start_position)
      return end_position.distance unless start_position
      return end_position.distance - start_position.distance if start_position.same_section?(end_position)
      track.section_for(start_position).remain_length(start_position.distance) + end_position.distance
    end

    def sum_length(sections)
      sections.inject(0) { |sum, section| sum + section.length }
    end

    def lanes
      track.lanes
    end

    def section(lane, number)
      lane.sections[number]
    end

    def distance_metric(to, from)
      Metrics::Distance.new(distance_between(to, from))
    end
  end
end