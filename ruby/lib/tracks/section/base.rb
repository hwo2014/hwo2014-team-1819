module Tracks::Section
  class Base
    attr_reader :lane_number, :section_number, :switchable, :neighbor_lines, :analytic, :length
    attr_reader :track_part, :next_section, :previous_section
    attr_accessor :turbo_support

    def initialize(lane_number, section_number, length)
      @lane_number, @section_number, @length = lane_number, section_number, length
      @switchable = false
      @neighbor_lines = []
      @analytic = {}
    end

    def remain_length(after)
      length - after
    end

    def set_analytic(type, value)
      analytic[type] = value
    end

    def set_track_part(part)
      @track_part = part
    end

    def set_next_section(section)
      @next_section = section
      section.set_previous_section(self)
    end

    def set_previous_section(section)
      @previous_section = section
    end

    def curve?
      false
    end

    def straight?
      false
    end

    def start?
      section_number == 0
    end

    def finish?
      next_section.start?
    end

    def switchable?
      @switchable
    end

    def contain?(position)
      lane_number == position.lane && section_number == position.section
    end

    def occupied_by?(cars)
      cars.any? { |car| car.position.lane == lane_number && car.position.section == section_number }
    end

    def similar_to?(section)
      self.class == section.class
    end

    def set_switch(neighbor_lines)
      @switchable = true
      @neighbor_lines = neighbor_lines
    end

    def distance_to_curve(distance)
      [(analytic[:lane_distance_to_curve] || 0) - distance, 0].max
    end

    def shortest_distance_to_finish
      analytic[:shortest_distance_to_finish]
    end

    def lane_distance_to_finish
      analytic[:lane_distance_to_finish]
    end

    def lane_distance_from_start
      analytic[:lane_distance_from_start]
    end

    def lane_distance_to_curve
      analytic[:lane_distance_to_curve]
    end

    def optimal_switches
      analytic[:optimal_switches]
    end

    def speed_limit
      analytic[:speed_limit]
    end

    def describe
      "lane#{lane_number}-section#{section_number}"
    end
  end
end
