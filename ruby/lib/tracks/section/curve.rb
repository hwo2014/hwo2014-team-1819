class Tracks::Section::Curve < Tracks::Section::Base
  attr_reader :radius, :angle

  def initialize(lane_number, section_number, radius, angle)
    super(lane_number, section_number, (2 * Math::PI * radius * angle / 360).abs)
    @radius, @angle = radius, angle
  end

  def curve?
    true
  end

  def similar_to?(section)
    super && radius == section.radius
  end

end