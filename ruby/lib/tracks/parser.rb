module Tracks
  class Parser
    attr_reader :track

    def initialize(data)
      @track = Tracks::Track.new
      parse(data)
    end

    def parse(data)
      traverse_data(data)
    end

    def traverse_data(data)
      data.each_pair do |key, value|
        parse_method = :"parse_#{key}"
        if respond_to?(parse_method)
          send(parse_method, value)
        elsif value.is_a?(Hash)
          traverse_data(value)
        end
      end
    end

    def parse_track(track_data)
      parse_lanes(track_data['lanes'])
      parse_sections(track_data['pieces'])
    end

    def parse_sections(sections_data)
      sections_data.each_with_index do |section, section_number|
        set_section(section)
        set_switch(section, section_number)
      end
    end

    def set_switch(section, section_number)
      return unless section['switch']
      track.set_switch(section_number)
    end

    def set_section(section)
      if section['radius']
        track.add_curve(section['radius'], section['angle'])
      else
        track.add_straight(section['length'])
      end
    end

    def parse_id(id)

    end

    def parse_lanes(lanes_data)
      lanes_data.each do |lane_data|
        track.add_lane(lane_data['index'], lane_data['distanceFromCenter'])
      end
    end

    def self.from_data(data)
      new(data).track
    end
  end
end
