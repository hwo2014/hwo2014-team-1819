module Tracks
  class Track
    include Tracks::Accessor
    extend Forwardable
    attr_reader :lanes, :analytic, :analyzer

    delegate [:distance_between, :prepare_for_race, :section_preceeds?, :sum_length] => :analyzer

    def initialize
      @lanes = []
      @analytic = {}
      @analyzer = Tracks::Analyzer.new(self)
    end

    def add_lane(number, offset)
      @lanes << Tracks::Lane.new(self, number, offset)
    end

    def add_straight(length)
      lanes.each { |lane| lane.add_straight(length) }
    end

    def add_curve(radius, angle)
      lanes.each { |lane| lane.add_curve(radius, angle) }
    end

    def set_switch(section_number)
      lanes.each { |lane| lane.set_switch(section_number, neighbor_lanes_number(lane.number)) }
    end

    def neighbor_lanes_number(current_lane)
      ordered = lanes.sort_by(&:offset)
      current = ordered.index { |lane| lane.number == current_lane }
      [get_with_boundaries(ordered, current - 1), get_with_boundaries(ordered, current + 1)].compact.map(&:number)
    end

    def direction_to_lane(from, to)
      offset_from = lanes[from].offset
      offset_to = lanes[to].offset
      if offset_to - offset_from > 0
        'Right'
      else
        'Left'
      end
    end

    def get_with_boundaries(collection, index)
      return nil if index < 0
      collection[index]
    end

    def average_length
      analytic[:average_length]
    end

    def distance_to_curve(lane, section, distance)
      lanes[lane].distance_to_curve(section, distance)
    end
  end
end
