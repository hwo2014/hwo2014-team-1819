class Tracks::Route < Array
  def last_section
    last
  end

  def add(section)
    self << section
  end

  def starts_from?(position)
    first && first.contain?(position)
  end

  def has_position?(position)
    any? { |section| section.contain?(position) }
  end

  def describe(sections = size)
    first(sections).map { |section| section.describe }
  end

  def starting_from(section)
    index = find_index(section)
    self[index..-1]
  end

  def clear_after(index)
    replace shift(index)
  end

  def current_part
    first.track_part
  end

  def next_curve_part(car_position)
    current = current_part
    target_section = find { |section| section.curve? && section.track_part != current }
    if target_section
      [target_section.track_part, distance_to(target_section) - car_position.distance]
    else
      [last.track_part, distance_to(last) - car_position.distance]
    end
  end

  def next_part(car_position)
    current = current_part
    target_section = find { |section| section.track_part != current }
    if target_section
      [target_section.track_part, distance_to(target_section) - car_position.distance]
    else
      [last.track_part, distance_to(last) - car_position.distance]
    end
  end

  def next_parts(car_position)
    current = current_part
    target_section = find { |section| section.track_part != current }
    next_parts = select { |section| section.track_part != current }.map(&:track_part).uniq
    [next_parts, distance_to(target_section) - car_position.distance]
  end

  def null_part
    @null_part ||= Tracks::Parts::Null.new
  end

  def distance_to(target_section)
    take_while { |section| section != target_section }.inject(0) { |sum, s| sum + s.length }
  end

  def distance_to_next(car_position)
    first.length - car_position.distance
  end

  def take_next_section(track)
    add(track.next_optimal_section(last.lane_number, last.section_number))
  end
end