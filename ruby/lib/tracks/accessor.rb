module Tracks
  module Accessor
    def section_for(position)
      get_section(position.lane, position.section)
    end

    def next_section_for(position)
      next_section(position.lane, position.section)
    end

    def next_optimal_section_for(position)
      next_optimal_section(position.lane, position.section)
    end

    def next_optimal_section(lane, section)
      current_section = get_section(lane, section)
      return next_section(lane, section) unless current_section.switchable?
      next_lane = current_section.optimal_switches.first
      next_section(next_lane, section)
    end

    def sections_till_switch(lane, section)
      [get_section(lane, section)] + sections_starting_from(lane, section + 1).take_while { |s| !s.switchable? }
    end

    def sections_count
      @sections_count ||= lanes.first.sections.count
    end

    def sections_starting_from(lane, section)
      lanes[lane].sections.rotate(section)
    end

    def sections_preceding_to(lane, section)
      sections_starting_from(lane, section).reverse
    end

    def sections_till_finish(lane, section)
      lanes[lane].sections[section .. sections_count]
    end

    def sections_from_start(lane, section)
      lanes[lane].sections[0 .. section]
    end

    def next_section(lane, section)
      lanes[lane].sections[section + 1] || lanes[lane].sections[0]
    end

    def get_section(lane, section)
      lanes[lane].sections[section]
    end

    def get_track_part_for_position(position)
      section_for(position).track_part
    end

    def next_curve_from(position)
      section = sections_starting_from(position.lane, position.section).find { |section| section.switchable? || section.curve? }
      if section.curve?
        section
      else
        next_curve_from(position.change_lane(section.optimal_switches.first))
      end
    end

    def optimal_sections_starting_from(position, count = 10)
      result = [section_for(position)]
      while result.count < count
        result << next_optimal_section(result.last.lane_number, result.last.section_number)
      end
      result
    end

    def next_curve_series_from(position)
      curve = next_curve_from(position)
      [curve] + sections_starting_from(curve.lane_number, curve.section_number).take_while { |section| section.curve? }
    end

    def next_switch_from(position)
      sections_starting_from(position.lane, position.section).find { |section| section.switchable? }
    end

    def possible_sections_from(lane, section)
      [next_section(lane, section)] + switchable_sections_from(lane, section)
    end

    def switchable_sections_from(lane, section)
      get_section(lane, section).neighbor_lines.map { |new_lane| next_section(new_lane, section) }
    end
  end
end