class Drivers::RandomThrottle < Drivers::Abstract
  def initialize(*args)
    super
    @random_throttle = 0.3 + rand(0.35)
  end

  def drive_track(tick)
    ['throttle', @random_throttle]
  end
end
