class Drivers::BestLiner < Drivers::Abstract
  attr_reader :navigator

  def initialize(*args)
    super
    @navigator = Cars::Navigator.new(race)
  end

  def drive_track(tick)
    navigator.switch_command || ['throttle', 0.3]
  end
end
