class Drivers::Lauda < Drivers::Abstract
  def drive_track(tick)
    navigator.update_routes
    position = car.position
    switch_command(position) || drive(position)
  end

  def switch_command(position)
    return nil unless (switch_command = navigator.switch_command)
    current_part = navigator.route.current_part
    next_parts, distance = navigator.route.next_parts(position)
    return switch_command unless current_part.strategy.going_to_crash?(car, next_parts, distance, 0)

    puts 'Crash on switch. Switch canceled!'
    nil
  end

  protected
  def drive(position)
    current_part = navigator.route.current_part
    next_parts, distance = navigator.route.next_parts(position)
    current_part.get_throttle(car, next_parts, distance)
  end
end
