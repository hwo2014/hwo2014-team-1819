class Drivers::Tester < Drivers::Abstract
  def initialize(*args)
    super
  end

  def drive_track(tick)
    throttle(tick)
  end

  def throttle(tick)
    ['throttle', 1]
  end

  def crashed(data)
    super
  end

  def abort_run(tick)
    return if false && car.position.section < 11
    puts "#{car.metrics.last(2).first.momentum.speed}\t#{car.position.angle}"
    car.save_metrics(race.config)
  end
end
