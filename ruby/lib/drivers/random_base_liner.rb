class Drivers::RandomBestLiner < Drivers::Abstract
  def initialize(*args)
    super
    @random_throttle = 0.3 + rand(35) / 100.0
  end

  def drive_track(tick)
    navigator.update_routes
    navigator.switch_command || ['throttle', @random_throttle]
  end
end
