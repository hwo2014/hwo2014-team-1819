module Drivers
  class Abstract
    attr_reader :race, :track, :car
    attr_reader :navigator

    def initialize(race, track, car)
      @race, @track, @car = race, track, car
      @navigator = Cars::Navigator.new(race)
    end

    def drive_track(tick)
      ['ping']
    end

    def crashed(data)
      car.save_metrics(race.config)
    end

    def lap_finished(data)
      car.save_metrics(race.config)
    end

    def tournament_end(data)
      car.save_metrics(race.config)
    end
  end
end
