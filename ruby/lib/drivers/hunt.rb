class Drivers::Hunt < Drivers::Abstract
  attr_reader :navigator, :throttle_control

  def initialize(*args)
    super
    @navigator = Cars::Navigator.new(race)
    @throttle_control = Physics::StraightThrottleControl.new(car.metrics)
  end

  def drive_track(tick)
    abort_run(car.position)
    navigator.switch_command || drive(car.position)
  end

  protected
  def abort_run(position)
    return unless false && position.section > 5
    puts "track finished at #{position.section} section"
    car.save_metrics(race.config)
    raise 'abort'
  end

  def drive(position)
    throttle = throttle_control.get_throttle(*next_speed_limit(position))
    ['throttle', throttle]
  end

  def next_speed_limit(position)
    curves = track.optimal_sections_starting_from(position)
    limited_curve = curves.find { |curve| curve.speed_limit }
    return [10, 10000] unless limited_curve
    previous_distance = track.sum_length(curves.take_while { |curve| !curve.speed_limit })
    speed_limit = limited_curve.speed_limit
    distance_to_limit = previous_distance - position.distance + speed_limit[0]
    return next_speed_limit(position.increase_section) if distance_to_limit < 0
    converted_limit = [speed_limit[1], distance_to_limit]
    converted_limit
  end
end
