module Configurations
  class Races
    attr_reader :server_host, :server_port
    def initialize(server_host, server_port)
      @server_host, @server_port = server_host, server_port
    end

    def bot_key
      'mpxz2tee+ffzaQ'
    end

    def bot_name
      'Mark Kotygoroshko'
    end

    def password
      'very_secure_111_password'
    end

    def join_data
      {:name => bot_name, :key => bot_key}
    end

    def save_metrics?
      false
    end

    def metrics_file
      'log/production-metrics.yml'
    end

    def metrics_size
      10
    end
  end
end
