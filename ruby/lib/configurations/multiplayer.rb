require "#{File.dirname(__FILE__)}/team_server"

module Configurations
  class Multiplayer < Configurations::TeamServer
    def bot_name
      "local_mult#{rand(100000)}"
    end

    def save_metrics?
      false
    end
  end
end
