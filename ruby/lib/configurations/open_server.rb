module Configurations
  class OpenServer
    def server_host
      %w(testserver.helloworldopen.com prost.helloworldopen.com hakkinen.helloworldopen.com senna.helloworldopen.com webber.helloworldopen.com).sample
    end

    def server_port
      '8091'
    end

    def bot_key
      'mpxz2tee+ffzaQ'
    end

    def bot_name
      'Mark Kotygoroshko'
    end

    def password
      '123'
    end

    def join_data
      {botId: {:name => bot_name, :key => bot_key}}
    end

    def save_metrics?
      false
    end

    def metrics_file
      'log/metrics.yml'
    end

    def metrics_size
      100000
    end
  end
end
