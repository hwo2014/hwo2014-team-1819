module Configurations
  class TeamServer
    def server_host
      #'testserver.helloworldopen.com'
      #'hakkinen.helloworldopen.com'
      'prost.helloworldopen.com'
      # 'senna.helloworldopen.com'
      #'webber.helloworldopen.com'
    end

    def server_port
      '8091'
    end

    def bot_key
      'mpxz2tee+ffzaQ'
    end

    def bot_name
      'Niki Lauda'
    end

    def password
      'very_secure_111_password'
    end

    def join_data
      {botId: {:name => bot_name, :key => bot_key}}
    end

    def save_metrics?
      true
    end

    def metrics_file
      'log/metrics.yml'
    end

    def metrics_size
      100000
    end
  end
end
