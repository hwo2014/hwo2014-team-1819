module Cars
  module Statistic
    attr_reader :crashes, :laps, :overall_rank, :fastest_rank, :best_lap, :active
    attr_reader :disabled, :disqualified

    def reset_statistic
      @disabled = false
      @crashes ||= 0
      @laps = 0
      @best_lap = 1000000
      @crashed_at = 0
      @overall_rank, @fastest_rank = 100, 100
    end

    def active?
      !crashed? && !disqualified
    end

    def crashed?
      @disabled && @crashed_at + 300 > metrics.last.tick
    end

    def crashed
      @disabled = true
      @crashed_at = metrics.last.tick
      @crashes = 1
    end

    def spawned
      @disabled = false
      @crashed_at = 0
    end

    def lap_finished(data)
      @laps = data[:laps]
      @overall_rank = data[:ranking]
      @fastest_rank = data[:fastest]
      @best_lap = data[:lap_time] if @best_lap > data[:lap_time]
    end

    def faster_then_me?
      overall_rank < race.car.overall_rank
    end

    def finished
      @disabled = true
    end

    def dnf
      @disqualified = true
    end

    def laps
      @laps || 0
    end

    def crashes
      @crashes || 0
    end
  end
end
