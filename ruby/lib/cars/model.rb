require "#{File.dirname(__FILE__)}/statistic"
require "#{File.dirname(__FILE__)}/turbo"

module Cars
  class Model
    attr_accessor :position
    attr_reader :color, :driver_name, :metrics, :probe, :race
    include Cars::Statistic
    include Cars::Turbo

    def initialize(color, driver_name, race)
      @color, @driver_name, @race = color, driver_name, race
      @metrics = Metrics::Race.new(metrics_size)
      @probe = Physics::Probe.new(metrics)
    end

    def set_dimensions(length, width, guide_flag)
      # code here
    end

    def set_position(position, tick)
      @position = position
      return unless tick
      metrics.add(Metrics::Drive.new(Metrics::Momentum.empty, position, distance_metric(race.track), tick))
      update_metrics
    end

    def prepare_for_race
      @metrics = Metrics::Race.new(metrics_size)
      @probe = Physics::Probe.new(metrics)
      reset_statistic
    end

    def physics
      @physics ||= race.physics
    end

    def has_color?(color)
      @color == color
    end

    def owned?
      false
    end

    def current_speed
      momentum.speed
    end

    def average_speed
      probe.average_speed
    end

    def momentum
      metrics.last.momentum
    end

    def distance
      metrics.last.distance
    end

    protected
    def update_metrics
    end

    def distance_metric(track)
      from_start = track.section_for(position).lane_distance_from_start + position.distance
      distance_delta = track.distance_between(position, metrics.last.position)
      metrics.last.distance.travel_by(distance_delta, from_start)
    end

    def metrics_size
      5
    end
  end
end