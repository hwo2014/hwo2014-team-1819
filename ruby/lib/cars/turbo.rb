module Cars
  module Turbo
    attr_reader :turbo, :turbo_factor, :turbo_duration

    def reset_statistic
      super
      @turbo = false
      @turbo_factor = 1
      @turbo_duration = 30
    end

    def turbo_available(factor, duration)
      @turbo_factor = factor
      @turbo_duration = duration
      @turbo = true
    end

    def turbo_start
      @turbo = false
    end

    def turbo_end
      @turbo_factor = 1
    end

    def has_turbo?
      @turbo && @turbo_factor > 1
    end

  end
end
