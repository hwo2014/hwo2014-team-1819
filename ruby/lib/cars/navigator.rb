class Cars::Navigator
  attr_reader :race, :car, :track, :route

  def initialize(race)
    @race = race
    @car, @track = race.car, race.track
    @route = Tracks::Route.new
  end

  def calculate_route
    @route.clear
    current_section = track.section_for(position)
    while route.size < max_route_size
      current_section = optimal_section(current_section, max_route_size - route.size)
      route.add(current_section)
      current_section = track.next_section(current_section.lane_number, current_section.section_number)
    end
  end

  def optimal_section(section, need_sections)
    return section unless section.switchable?
    next_lane = find_clear_route(section, need_sections) || section.optimal_switches.first
    track.get_section(next_lane, section.section_number)
  end

  def find_clear_route(section, need_sections)
    return nil unless race.opponents.size > 0
    section.optimal_switches.find do |next_lane|
      sections = track.sections_starting_from(next_lane, section.section_number)[0..need_sections]
      !road_blocked?(sections)
    end
  end

  def road_blocked?(sections)
    race.opponents.any? do |opponent|
      position = opponent.position
      sections.any? { |section| section.contain?(position) }
    end
  end

  def update_routes
    return if route.starts_from?(position)
    build_optimal_route(position.lane, position.section)
    @switching_to_lane = nil
  end

  def build_optimal_route(lane, section, starting_from = 0)
    route.clear_after(starting_from)
    current_section = track.get_section(lane, section)
    while route.size < max_route_size
      route.add(current_section)
      current_section = track.next_section(current_section.lane_number, current_section.section_number)
      current_section = track.get_section(current_section.optimal_switches.first, current_section.section_number) if current_section.switchable?
    end
  end

  def position
    car.position
  end

  def should_switch?
    next_section.lane_number != position.lane
  end

  def can_switch?
    return false unless next_section.switchable?
    return false if route.distance_to_next(position) > car.current_speed * 2.5
    true
  end

  def next_section
    route[1]
  end

  def current_section
    route[0]
  end

  def switch_command
    return nil unless can_switch?
    return nil if already_switched?(next_section.lane_number)
    rebuild_blocked_route
    switching_to_lane(next_section.lane_number)
  end

  def rebuild_blocked_route
    return if route_clear?
    possible_switch = get_unblocked_lane
    build_optimal_route(possible_switch, next_section.section_number, 1) if possible_switch
  end

  def get_unblocked_lane1
    track.get_section(current_section.lane_number, next_section.section_number).optimal_switches.find do |lane|
      sections = track.sections_till_switch(lane, next_section.section_number)
      opponent = first_opponent_on(sections)
      !can_catchup_opponent?(opponent, sections)
    end
  end

  def get_unblocked_lane
    lanes_status = track.get_section(current_section.lane_number, next_section.section_number).optimal_switches.map do |lane|
      sections = track.sections_till_switch(lane, next_section.section_number)
      opponent = first_opponent_on(sections)
      [lane, *catchup_parameters(opponent, sections)]
    end

    speed_sorted = lanes_status.sort { |x, y| [y[2], y[1]] <=> [x[2], x[1]] }
    speed_sorted.first[0]
  end

  def catchup_parameters(opponent, sections)
    return [0, 0] unless opponent
    speed_delta = car.average_speed - opponent.average_speed
    time_delta = time_to_drive(sections, car) - time_to_drive(sections, opponent)
    return [speed_delta, 10000] if time_delta > 50
    return [0, time_delta] if -0.2 < speed_delta && speed_delta < 0.2
    [speed_delta, time_delta]
  end

  def can_catchup_opponent?(opponent, sections)
    return false unless opponent
    return false if (opponent.average_speed - car.average_speed) > 0.5
    return false if (time_to_drive(sections, opponent) - time_to_drive(sections, car)) < -20
    true
  end

  def time_to_drive(sections, opponent)
    distance = sections.inject(0) { |sum, s| sum + s.length } + sections.first.lane_distance_from_start
    distance / opponent.average_speed
  end

  def possible_opponents_for(sections)
    section_numbers = sections.map(&:section_number)
    race.opponents.select { |car| section_numbers.include?(car.position.section) }
  end

  def first_opponent_on(sections)
    lane = sections.first.lane_number
    section_numbers = sections.map(&:section_number)
    opponents_on_lane = opponents.select { |car| car.active? && car.position.lane == lane && section_numbers.include?(car.position.section) }
    opponents_on_lane.sort { |x, y| x.position.section <=> y.position.section }.first
  end

  def route_clear?
    next_sections = [next_section] + route.rotate(2).take_while { |section| !section.switchable? }
    section_numbers = next_sections.map(&:section_number)
    possible_opponents = opponents.select { |car| section_numbers.include?(car.position.section) }
    possible_opponents.none? { |car| car.position.lane == next_section.lane_number }
  end

  def opponents
    race.opponents
  end

  def switching_to_lane(next_lane)
    return nil if next_lane == current_section.lane_number
    @switching_to_lane = next_lane
    switch_direction = track.direction_to_lane(position.lane, next_lane)
    ['switchLane', switch_direction]
  end

  def already_switched?(target)
    @switching_to_lane == target
  end

  def max_route_size
    30
  end
end