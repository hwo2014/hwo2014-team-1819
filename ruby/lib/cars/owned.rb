class Cars::Owned < Cars::Model
  def update_metrics
    probe.update_momentum
    probe.update_linear_coefficients(physics)
    probe.update_angular_coefficients(physics, race.track.section_for(position))
    #probe.print_centripetial_force(physics, race.track.section_for(position))
  end

  def set_throttle(throttle)
    metrics.last.momentum.throttle = throttle
  end

  def save_metrics(config)
    metrics.save_to_file(config.metrics_file) if config.save_metrics?
  end

  def metrics_size
    race.config.metrics_size
  end

  def crashed
    puts 'We crashed :('
    race.track.section_for(position).track_part.strategy.part_crashed(self)
    save_metrics(race.config)
    super
  end

  def lap_finished(data)
    puts '-' * 20
    puts "We finished lap #{data[:laps]} with time #{data[:lap_time]}."
    puts "Current standings: overall: #{data[:ranking]}, fastest #{data[:fastest]}."
    puts '-' * 20
    save_metrics(race.config)
    super
  end

  def turbo_start
    super
    physics.enable_turbo(turbo_factor)
  end

  def turbo_end
    super
    physics.disable_turbo
  end

  def owned?
    true
  end
end