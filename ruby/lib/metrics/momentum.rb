module Metrics
  Momentum = Struct.new(:throttle, :speed, :acceleration, :angular_speed, :angular_acceleration) do
    def self.empty
      Metrics::Momentum.new(0, 0, 0, 0, 0)
    end

    def moving?
      speed > 0
    end

    def same_throttle?(momentum)
      throttle == momentum.throttle
    end
  end
end

