require 'yaml'

module Metrics
  class Race
    extend ::Forwardable

    attr_reader :metrics, :metrics_size
    delegate [:last, :size, :[]] => :metrics

    def initialize(metrics_size = 10)
      @metrics_size = metrics_size
      @metrics = [Metrics::Drive.empty]
    end

    def add(metric)
      metrics.shift if size > metrics_size
      metrics << metric
    end

    def set_throttle(amount)
      last.momentum.throttle = amount
    end

    def get_by_tick(tick)
      metrics.find { |metric| metric.tick == tick }
    end

    def last_metrics(count)
      metrics.last(count)
    end

    def previous
      metrics[-2]
    end

    def save_to_file(file)
      arr = metrics.map do |metric|
        {
            tick: metric.tick,
            momentum: metric.momentum.to_h,
            position: metric.position.to_h,
            distance: metric.distance.to_h
        }
      end
      File.open(file, 'w') { |file|  file.puts arr.to_yaml }
      File.open("#{file}.js", 'w') { |file|  file.puts "var metricsData = #{JSON.generate(arr)}" }
    end
  end
end

