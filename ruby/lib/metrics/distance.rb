module Metrics
  Distance = Struct.new(:absolute, :from_start) do
    def self.empty
      Metrics::Distance.new(0)
    end

    def travel_by(delta, distance_from_start)
      Metrics::Distance.new(absolute + delta, distance_from_start)
    end
  end
end

