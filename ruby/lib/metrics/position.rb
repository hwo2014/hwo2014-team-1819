module Metrics
  Position = Struct.new(:lane, :section, :distance, :angle, :lap, :start_lane) do
    def self.empty
      Metrics::Position.new(0, 0, 0, 0, -1, 0)
    end

    def same_section?(position)
      position.section == section
    end

    def switching_lane?
      lane != start_lane
    end

    def change_lane(new_lane)
      Metrics::Position.new(new_lane, section, distance)
    end

    def increase_section
      Metrics::Position.new(lane, section + 1, distance)
    end
  end
end

