module Metrics
  Drive = Struct.new(:momentum, :position, :distance, :tick) do
    def self.empty
      Metrics::Drive.new(Metrics::Momentum.empty, Metrics::Position.empty, Metrics::Distance.empty, 0)
    end
  end
end

