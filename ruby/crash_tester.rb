require 'json'
require 'socket'
require 'forwardable'
require 'pry'

Dir[File.expand_path(File.join(File.dirname(__FILE__), 'lib', '**', '*.rb'))].each { |f| require f }

# NoobBot.new(server_host, server_port, bot_name, bot_key)
tracks = %w(keimola germany usa france elaeintarha imola england suzuka)
loop do
  race = Games::Tournament.new(Drivers::Lauda, Configurations::OpenServer.new)
  race.start(tracks.sample, 1, false)
end
