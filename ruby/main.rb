require 'json'
require 'socket'
require 'forwardable'

Dir[File.expand_path(File.join(File.dirname(__FILE__), 'lib', '**', '*.rb'))].sort.each { |f| require f }

server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]

race = Games::Tournament.new(Drivers::Lauda, Configurations::Races.new(server_host, server_port))
race.join
