def stub_driver
  driver = double(Drivers::Abstract, :drive_track => ['forward', 1])
  driver.stub(:new => driver)
  driver
end