def stub_communicator
  communicator = double(Games::Communicator, :communicate => nil)
  Games::Communicator.stub(:new => communicator)
  communicator
end