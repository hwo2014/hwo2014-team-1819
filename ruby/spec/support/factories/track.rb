def create_track(lanes, offset = 20)
  track = Tracks::Track.new
  lanes.times do |index|
    track.add_lane(index, offset * index)
  end
  track
end

def add_sample_sections(track)
  track.add_straight(100)
  track.add_straight(100)
  track.add_curve(100, 90)
  track.add_straight(100)
  track.add_straight(100)
  track.set_switch(1)
  track.set_switch(3)
  track.prepare_for_race(create_race(track))
  track
end