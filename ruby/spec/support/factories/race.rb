def create_race(track = nil)
  double(Games::Tournament, :config => Configurations::TeamServer.new, :physics => Physics::Coefficients.new, :track => track)
end
