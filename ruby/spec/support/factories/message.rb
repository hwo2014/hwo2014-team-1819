def simple_position_message(lane = 0, section = 0, distance = 0)
  {'msgType' => 'carPositions',
   'data' => [
       {'piecePosition' => {'pieceIndex' => section, 'inPieceDistance' => distance,
                            'lane' => {'startLaneIndex' => lane, 'endLaneIndex' => lane}}}]}
end