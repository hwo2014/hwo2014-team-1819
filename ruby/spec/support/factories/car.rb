def create_owned_car(lane, section, distance = 0)
  car = Cars::Owned.new('red', 'test_car', create_race)
  car.position = Metrics::Position.new(lane, section, distance)
  car
  end

def create_opponent_car(lane, section, distance = 0, average_speed = 2)
  car = Cars::Opponent.new('blue', 'test_opponent', create_race)
  car.position = Metrics::Position.new(lane, section, distance)
  car.stub(:average_speed => average_speed)
  car
end