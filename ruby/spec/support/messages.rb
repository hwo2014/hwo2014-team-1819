def create_game_response
  {"msgType" => "createRace", "data" => {"botId" => {"name" => "Addison Johnston", "key" => "mpxz2tee+ffzaQ"}, "trackName" => "keimola", "carCount" => 1}}
end

def your_car_response
  {"msgType" => "yourCar", "data" => {"name" => "Addison Johnston", "color" => "red"}, "gameId" => "07d0a0f6-9662-4bfd-880f-42268a66fafa"}
end

def game_init_response
  {"msgType" => "gameInit",
   "data" => {
       "race" => {"track" => {"id" => "keimola", "name" => "Keimola", "pieces" => [{"length" => 100.0}, {"length" => 100.0}, {"length" => 100.0}, {"length" => 100.0, "switch" => true}, {"radius" => 100, "angle" => 45.0}, {"radius" => 100, "angle" => 45.0}, {"radius" => 100, "angle" => 45.0}, {"radius" => 100, "angle" => 45.0}, {"radius" => 200, "angle" => 22.5, "switch" => true}, {"length" => 100.0}, {"length" => 100.0}, {"radius" => 200, "angle" => -22.5}, {"length" => 100.0}, {"length" => 100.0, "switch" => true}, {"radius" => 100, "angle" => -45.0}, {"radius" => 100, "angle" => -45.0}, {"radius" => 100, "angle" => -45.0}, {"radius" => 100, "angle" => -45.0}, {"length" => 100.0, "switch" => true}, {"radius" => 100, "angle" => 45.0}, {"radius" => 100, "angle" => 45.0}, {"radius" => 100, "angle" => 45.0}, {"radius" => 100, "angle" => 45.0}, {"radius" => 200, "angle" => 22.5}, {"radius" => 200, "angle" => -22.5}, {"length" => 100.0, "switch" => true}, {"radius" => 100, "angle" => 45.0}, {"radius" => 100, "angle" => 45.0}, {"length" => 62.0}, {"radius" => 100, "angle" => -45.0, "switch" => true}, {"radius" => 100, "angle" => -45.0}, {"radius" => 100, "angle" => 45.0}, {"radius" => 100, "angle" => 45.0}, {"radius" => 100, "angle" => 45.0}, {"radius" => 100, "angle" => 45.0}, {"length" => 100.0, "switch" => true}, {"length" => 100.0}, {"length" => 100.0}, {"length" => 100.0}, {"length" => 90.0}], "lanes" => [{"distanceFromCenter" => -10, "index" => 0}, {"distanceFromCenter" => 10, "index" => 1}], "startingPoint" => {"position" => {"x" => -300.0, "y" => -44.0}, "angle" => 90.0}},
                  "cars" => [{"id" => {"name" => "Addison Johnston", "color" => "red"}, "dimensions" => {"length" => 40.0, "width" => 20.0, "guideFlagPosition" => 10.0}}],
                  "raceSession" => {"laps" => 3, "maxLapTimeMs" => 60000, "quickRace" => true}}},
   "gameId" => "07d0a0f6-9662-4bfd-880f-42268a66fafa"}
end

def single_car_position_message(tick = nil)
  {"msgType" => "carPositions",
   "data" => [{
                  "id" => {"name" => "Addison Johnston", "color" => "red"},
                  "angle" => 0.0,
                  "piecePosition" => {"pieceIndex" => 0, "inPieceDistance" => 0.0, "lane" => {"startLaneIndex" => 0, "endLaneIndex" => 0}, "lap" => 0}
              }],
   "gameId" => "07d0a0f6-9662-4bfd-880f-42268a66fafa", "gameTick" => tick}
end

def game_start_message
  {"msgType" => "gameStart", "data" => nil, "gameId" => "07d0a0f6-9662-4bfd-880f-42268a66fafa", "gameTick" => 0}
end