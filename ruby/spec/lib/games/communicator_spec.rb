require 'spec_helper'

describe Games::Communicator do
  before do
    @socket = double(:socket, :gets => nil, :puts => nil)
    TCPSocket.stub(:open => @socket)
    @config = Configurations::TeamServer.new
    @communicator = Games::Communicator.new(@config)
  end

  it 'should send join on init' do
    @config.stub(:join_data => {join: 2})
    @socket.should_receive(:puts).with(JSON.generate({:msgType => 'createRace', :data => {join: 2, my_data: 1}}))
    @communicator.init('createRace', {my_data: 1})
  end

  it 'should raise error on communicate without init' do
    expect { @communicator.communicate }.to raise_error('not initialized')
  end

  describe 'after init' do
    before do
      @communicator.init('track', {})
    end

    it 'should read from socket' do
      @socket.should_receive(:gets).and_return(JSON.generate({:msgType => 'gameStart', :data => {:test => 1}}), nil)
      @communicator.communicate do |type, data|
        expect(type).to eq('gameStart')
        expect(data).to eq({'msgType' => 'gameStart', 'data' => {'test' => 1}})
      end
    end

    it 'should send response to socket' do
      @socket.should_receive(:gets).and_return(JSON.generate({:msgType => 'gameStart', :data => {:test => 1}}), nil)
      @socket.should_receive(:puts).with(JSON.generate({:msgType => 'ping', :data => {:test => true}}))
      @communicator.communicate { ['ping', {:test => true}] }
    end
  end
end