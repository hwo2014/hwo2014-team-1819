require 'spec_helper'

describe Games::Tournament do
  before do
    @config = Configurations::TeamServer.new
    @communicator = stub_communicator
    @driver = stub_driver
    @tournament = Games::Tournament.new(@driver, @config)
  end

  describe 'starting' do
    it 'should initialize communicator with correct parameters' do
      @communicator.should_receive(:init).with('createRace', {trackName: 'keimola', carCount: 2})
      @tournament.start('keimola', 2, false)
    end

    it 'should initialize communicator with correct parameters' do
      @communicator.should_receive(:init).with('joinRace', {trackName: 'keimola', carCount: 3})
      @tournament.start('keimola', 3, true)
    end
  end

  describe 'handling events' do
    describe 'create race' do
      it 'should respond with ping' do
        expect(@tournament.handle_communication('createRace', create_game_response)).to respond_with('ping')
      end
    end

    describe 'your car' do
      it 'should respond with ping' do
        expect(@tournament.handle_communication('yourCar', your_car_response)).to respond_with('ping')
      end

      it 'should create car' do
        expect { @tournament.handle_communication('yourCar', your_car_response) }.to change(@tournament, :car).to(be_kind_of(Cars::Owned))
      end

      it 'should create car with correct color' do
        @tournament.handle_communication('yourCar', your_car_response)
        expect(@tournament.car.color).to eq('red')
      end
    end

    describe 'game init' do
      before do
        @tournament.handle_communication('yourCar', your_car_response)
      end

      it 'should respond with ping' do
        expect(@tournament.handle_communication('gameInit', game_init_response)).to respond_with('ping')
      end

      it 'should parse track' do
        expect { @tournament.handle_communication('gameInit', game_init_response) }.to change(@tournament, :track).to(be_kind_of(Tracks::Track))
      end

      it 'should parse track' do
        expect { @tournament.handle_communication('gameInit', game_init_response) }.to change(@tournament, :driver).to(@driver)
      end

      it 'should add participants' do
        expect { @tournament.handle_communication('gameInit', game_init_response) }.to change { @tournament.participants.count }.by(1)
        @tournament.participants.should include(@tournament.car)
      end
    end

    describe 'car positions' do
      before do
        @tournament.handle_communication('yourCar', your_car_response)
        @tournament.handle_communication('gameInit', game_init_response)
      end

      it 'should respond with ping while not started' do
        expect(@tournament.handle_communication('createRace', create_game_response)).to respond_with('ping')
      end

      it 'should update car position' do
        expect { @tournament.handle_communication('carPositions', single_car_position_message) }.to change(@tournament.car, :position)
      end

      it 'should not drive car' do
        @driver.should_not_receive(:drive)
        expect { @tournament.handle_communication('carPositions', single_car_position_message) }.to change(@tournament.car, :position)
      end
    end

    describe 'game start' do
      before do
        @tournament.handle_communication('yourCar', your_car_response)
        @tournament.handle_communication('gameInit', game_init_response)
        @tournament.handle_communication('carPositions', single_car_position_message)
      end

      it 'should not update race status without tick' do
        expect { @tournament.handle_communication('gameStart', game_start_message) }.to_not change { @tournament.race_started?(nil) }
      end

      it 'should update race status with tick' do
        expect { @tournament.handle_communication('gameStart', game_start_message) }.to change { @tournament.race_started?(0) }
      end

      it 'should ask to drive car' do
        @driver.should_receive(:drive_track).with(0)
        @tournament.handle_communication('gameStart', game_start_message)
      end

      it 'should respond with driver response' do
        expect(@tournament.handle_communication('gameStart', game_start_message)).to respond_with('forward', 1)
      end
    end

    describe 'car positions after start' do
      before do
        @tournament.handle_communication('yourCar', your_car_response)
        @tournament.handle_communication('gameInit', game_init_response)
        @tournament.handle_communication('carPositions', single_car_position_message)
        @tournament.handle_communication('gameStart', game_start_message)
      end

      it 'should ask to drive car' do
        @driver.should_receive(:drive_track).with(1)
        @tournament.handle_communication('carPositions', single_car_position_message(1))
      end

      it 'should respond with driver response' do
        expect(@tournament.handle_communication('carPositions', single_car_position_message(1))).to respond_with('forward', 1)
      end
    end
  end
end