require 'spec_helper'

describe Cars::Navigator do
  before do
    @track = add_sample_sections(create_track(3))
    @car = create_owned_car(1, 0)
    @race = double(:race, :car => @car, :opponents => [], :track => @track)
    @navigator = Cars::Navigator.new(@race)
  end

  def update_route(car_lane, average_speed = 5)
    @car.position = Metrics::Position.new(car_lane, 0, 99)
    @car.stub(:average_speed => average_speed, :current_speed => 2)
    @navigator.update_routes
  end

  describe 'route without opponents' do
    describe 'default route' do
      it 'should return shortest route for lane 0' do
        update_route(0)
        expect(@navigator.route.describe(5)).to eq(%w(lane0-section0 lane1-section1 lane1-section2 lane1-section3 lane1-section4))
      end

      it 'should return shortest route for lane 1' do
        update_route(1)
        expect(@navigator.route.describe(5)).to eq(%w(lane1-section0 lane2-section1 lane2-section2 lane2-section3 lane2-section4))
      end

      it 'should return shortest route for lane 3' do
        update_route(2)
        expect(@navigator.route.describe(5)).to eq(%w(lane2-section0 lane2-section1 lane2-section2 lane2-section3 lane2-section4))
      end
    end

    describe 'switching time' do
      it 'should switch to 1st lane from 0' do
        update_route(0, 10)
        expect(@navigator.switch_command).to eq(['switchLane', 'Right'])
      end

      it 'should switch to 2nd lane from 1' do
        update_route(1, 20)
        expect(@navigator.switch_command).to eq(['switchLane', 'Right'])
      end

      it 'should not switch from 3rd' do
        update_route(2, 20)
        expect(@navigator.switch_command).to eq(nil)
      end
    end
  end

  describe 'route with opponent which goes slower' do
    describe 'one lane blocked' do
      describe 'car is inactive' do
        before do
          @opponents = [create_opponent_car(1, 1, 10)]
          @opponents.first.stub(:race => @race)
          @opponents.first.crashed
          @race.stub(opponents: @opponents)
        end

        it 'should return shortest route for lane 0' do
          update_route(0)
          expect(@navigator.route.describe(5)).to eq(%w(lane0-section0 lane1-section1 lane1-section2 lane1-section3 lane1-section4))
        end

        it 'should return shortest route for lane 1' do
          update_route(1)
          expect(@navigator.route.describe(5)).to eq(%w(lane1-section0 lane2-section1 lane2-section2 lane2-section3 lane2-section4))
        end

        it 'should return shortest route for lane 3' do
          update_route(2)
          expect(@navigator.route.describe(5)).to eq(%w(lane2-section0 lane2-section1 lane2-section2 lane2-section3 lane2-section4))
        end

        describe 'switching commands' do
          it 'should switch to 1st lane from 0' do
            update_route(0, 10)
            expect(@navigator.switch_command).to eq(['switchLane', 'Right'])
            expect(@navigator.route.describe(5)).to eq(%w(lane0-section0 lane1-section1 lane1-section2 lane1-section3 lane1-section4))
          end

          it 'should switch to 2nd lane from 1' do
            update_route(1, 20)
            expect(@navigator.switch_command).to eq(['switchLane', 'Right'])
            expect(@navigator.route.describe(5)).to eq(%w(lane1-section0 lane2-section1 lane2-section2 lane2-section3 lane2-section4))
          end

          it 'should not switch from 3rd' do
            update_route(2, 20)
            expect(@navigator.switch_command).to eq(nil)
            expect(@navigator.route.describe(5)).to eq(%w(lane2-section0 lane2-section1 lane2-section2 lane2-section3 lane2-section4))
          end
        end
      end

      describe 'can catchup and overtake' do
        before do
          @opponents = [create_opponent_car(1, 1, 10)]
          @race.stub(opponents: @opponents)
        end

        it 'should return shortest route for lane 0' do
          update_route(0)
          expect(@navigator.route.describe(5)).to eq(%w(lane0-section0 lane1-section1 lane1-section2 lane1-section3 lane1-section4))
        end

        it 'should return shortest route for lane 1' do
          update_route(1)
          expect(@navigator.route.describe(5)).to eq(%w(lane1-section0 lane2-section1 lane2-section2 lane2-section3 lane2-section4))
        end

        it 'should return shortest route for lane 3' do
          update_route(2)
          expect(@navigator.route.describe(5)).to eq(%w(lane2-section0 lane2-section1 lane2-section2 lane2-section3 lane2-section4))
        end

        describe 'switching commands' do
          it 'should not switch to 1st lane from 0' do
            update_route(0, 10)
            expect(@navigator.switch_command).to eq(nil)
            expect(@navigator.route.describe(5)).to eq(%w(lane0-section0 lane0-section1 lane0-section2 lane0-section3 lane0-section4))
          end

          it 'should switch to 2nd lane from 1' do
            update_route(1, 20)
            expect(@navigator.switch_command).to eq(['switchLane', 'Right'])
            expect(@navigator.route.describe(5)).to eq(%w(lane1-section0 lane2-section1 lane2-section2 lane2-section3 lane2-section4))
          end

          it 'should not switch from 3rd' do
            update_route(2, 20)
            expect(@navigator.switch_command).to eq(nil)
            expect(@navigator.route.describe(5)).to eq(%w(lane2-section0 lane2-section1 lane2-section2 lane2-section3 lane2-section4))
          end
        end
      end

      describe 'cannot catchup' do
        before do
          @opponents = [create_opponent_car(1, 3, 60)]
          @race.stub(opponents: @opponents)
        end

        it 'should switch to 1st lane from 0' do
          update_route(0)
          expect(@navigator.switch_command).to eq(['switchLane', 'Right'])
          expect(@navigator.route.describe(5)).to eq(%w(lane0-section0 lane1-section1 lane1-section2 lane1-section3 lane1-section4))
        end

        it 'should switch to 2nd lane from 1' do
          update_route(1)
          expect(@navigator.switch_command).to eq(['switchLane', 'Right'])
          expect(@navigator.route.describe(5)).to eq(%w(lane1-section0 lane2-section1 lane2-section2 lane2-section3 lane2-section4))
        end

        it 'should not switch from 3rd' do
          update_route(2, 0.5)
          expect(@navigator.switch_command).to eq(nil)
          expect(@navigator.route.describe(5)).to eq(%w(lane2-section0 lane2-section1 lane2-section2 lane2-section3 lane2-section4))
        end
      end
    end

    describe 'route with opponents which goes faster' do
      before do
        @opponents = [create_opponent_car(1, 1, 10, 6), create_opponent_car(1, 2, 30, 7)]
        @race.stub(opponents: @opponents)
      end

      it 'should switch to 1st lane from 0' do
        update_route(0)
        expect(@navigator.switch_command).to eq(['switchLane', 'Right'])
        expect(@navigator.route.describe(5)).to eq(%w(lane0-section0 lane1-section1 lane1-section2 lane1-section3 lane1-section4))
      end

      it 'should switch to 2nd lane from 1' do
        update_route(1)
        expect(@navigator.switch_command).to eq(['switchLane', 'Right'])
        expect(@navigator.route.describe(5)).to eq(%w(lane1-section0 lane2-section1 lane2-section2 lane2-section3 lane2-section4))
      end

      it 'should not switch from 3rd' do
        update_route(2)
        expect(@navigator.switch_command).to eq(nil)
        expect(@navigator.route.describe(5)).to eq(%w(lane2-section0 lane2-section1 lane2-section2 lane2-section3 lane2-section4))
      end
    end

    describe 'route with opponents which goes slower no chance to overtake' do
      before do
        @opponents = [create_opponent_car(1, 1, 10, 3), create_opponent_car(2, 1, 30, 3)]
        @race.stub(opponents: @opponents)
      end

      it 'should not switch to 1st lane from 0' do
        update_route(0)
        expect(@navigator.switch_command).to eq(nil)
        expect(@navigator.route.describe(5)).to eq(%w(lane0-section0 lane0-section1 lane0-section2 lane0-section3 lane0-section4))
      end

      it 'should switch to 1st lane from 2nd' do
        update_route(1)
        expect(@navigator.switch_command).to eq(['switchLane', 'Left'])
        expect(@navigator.route.describe(5)).to eq(%w(lane1-section0 lane0-section1 lane0-section2 lane0-section3 lane0-section4))
      end

      it 'should not switch from 3rd' do
        update_route(2)
        expect(@navigator.switch_command).to eq(nil)
        expect(@navigator.route.describe(5)).to eq(%w(lane2-section0 lane2-section1 lane2-section2 lane2-section3 lane2-section4))
      end
    end

    describe 'route with slower and faster opponents' do
      before do
        @opponents = [create_opponent_car(1, 1, 10, 6), create_opponent_car(2, 2, 30, 2)]
        @race.stub(opponents: @opponents)
      end

      it 'should switch to 1st lane from 0' do
        update_route(0)
        expect(@navigator.switch_command).to eq(['switchLane', 'Right'])
        expect(@navigator.route.describe(5)).to eq(%w(lane0-section0 lane1-section1 lane1-section2 lane1-section3 lane1-section4))
      end

      it 'should switch not to 2nd lane from 1' do
        update_route(1)
        expect(@navigator.switch_command).to eq(nil)
        expect(@navigator.route.describe(5)).to eq(%w(lane1-section0 lane1-section1 lane1-section2 lane1-section3 lane1-section4))
      end

      it 'should not switch from 3rd' do
        update_route(2)
        expect(@navigator.switch_command).to eq(['switchLane', 'Left'])
        expect(@navigator.route.describe(5)).to eq(%w(lane2-section0 lane1-section1 lane1-section2 lane1-section3 lane1-section4))
      end
    end

    describe 'route with two slow opponents ahead' do
      describe 'slower goes on optimal' do
        before do
          @opponents = [create_opponent_car(1, 1, 10, 4), create_opponent_car(2, 1, 30, 3)]
          @race.stub(opponents: @opponents)
        end

        it 'should not switch from 1st lane' do
          update_route(0)
          expect(@navigator.switch_command).to eq(nil)
        end

        it 'should switch from 2nd lane to 1st' do
          update_route(1)
          expect(@navigator.switch_command).to eq(['switchLane', 'Left'])
        end

        it 'should switch from 3rd lane to 2nd' do
          update_route(2)
          expect(@navigator.switch_command).to eq(['switchLane', 'Left'])
        end
      end

      describe 'slower goes on suboptimal' do
        before do
          @opponents = [create_opponent_car(1, 1, 10, 3), create_opponent_car(2, 1, 30, 4)]
          @race.stub(opponents: @opponents)
        end

        it 'should not switch from 1st lane' do
          update_route(0)
          expect(@navigator.switch_command).to eq(nil)
        end

        it 'should switch from 2nd lane to 1st' do
          update_route(1)
          expect(@navigator.switch_command).to eq(['switchLane', 'Left'])
        end

        it 'should not switch from 3rd lane' do
          update_route(2)
          expect(@navigator.switch_command).to eq(nil)
        end
      end

      describe 'slower far ahead on optimal' do
        before do
          @opponents = [create_opponent_car(1, 1, 10, 4), create_opponent_car(2, 3, 30, 1)]
          @race.stub(opponents: @opponents)
        end

        it 'should not switch from 1st lane' do
          update_route(0)
          expect(@navigator.switch_command).to eq(nil)
        end

        it 'should switch from 2nd lane to 2nd' do
          update_route(1)
          expect(@navigator.switch_command).to eq(['switchLane', 'Right'])
        end

        it 'should not switch from 3rd lane' do
          update_route(2)
          expect(@navigator.switch_command).to eq(nil)
        end
      end
    end

    describe 'route with opponent goes behind and faster' do
      before do
        @opponents = [create_opponent_car(1, 0, 10, 10), create_opponent_car(2, 0, 80, 10)]
        @race.stub(opponents: @opponents)
      end

      it 'should switch to 1st lane from 0' do
        update_route(0)
        expect(@navigator.switch_command).to eq(['switchLane', 'Right'])
        expect(@navigator.route.describe(5)).to eq(%w(lane0-section0 lane1-section1 lane1-section2 lane1-section3 lane1-section4))
      end

      it 'should switch not to 2nd lane from 1' do
        update_route(1)
        expect(@navigator.switch_command).to eq(['switchLane', 'Right'])
        expect(@navigator.route.describe(5)).to eq(%w(lane1-section0 lane2-section1 lane2-section2 lane2-section3 lane2-section4))
      end

      it 'should not switch from 3rd' do
        update_route(2)
        expect(@navigator.switch_command).to eq(nil)
        expect(@navigator.route.describe(5)).to eq(%w(lane2-section0 lane2-section1 lane2-section2 lane2-section3 lane2-section4))
      end
    end
  end
end