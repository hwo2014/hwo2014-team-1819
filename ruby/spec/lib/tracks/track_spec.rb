require 'spec_helper'

describe Tracks::Track do
  before do
    @track = Tracks::Track.new
  end

  describe 'neighbor lines' do
    before do
      @track.add_lane(0, 0)
      @track.add_lane(1, 20)
      @track.add_lane(2, -20)
      @track.add_lane(3, 40)
    end

    it 'should give correct neighbor lanes for 0 lane' do
      @track.neighbor_lanes_number(0).should == [2, 1]
    end

    it 'should give correct neighbor lanes for 1st lane' do
      @track.neighbor_lanes_number(1).should == [0, 3]
    end

    it 'should give correct neighbor lanes for 2nd lane' do
      @track.neighbor_lanes_number(2).should == [0]
    end

    it 'should give correct neighbor lanes for 3d lane' do
      @track.neighbor_lanes_number(3).should == [1]
    end

    it 'should have correct direction for left' do
      expect(@track.direction_to_lane(0, 2)).to eq('Left')
      expect(@track.direction_to_lane(1, 2)).to eq('Left')
      expect(@track.direction_to_lane(3, 0)).to eq('Left')
    end

    it 'should have correct direction for right' do
      expect(@track.direction_to_lane(2, 0)).to eq('Right')
      expect(@track.direction_to_lane(1, 3)).to eq('Right')
      expect(@track.direction_to_lane(0, 3)).to eq('Right')
    end
  end
end