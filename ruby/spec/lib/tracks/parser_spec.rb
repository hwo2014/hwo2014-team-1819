require 'spec_helper'

describe Tracks::Parser do
  describe 'keimola track' do
    before do
      data = JSON.parse('{"race":{"track":{"id":"keimola","name":"Keimola","pieces":[{"length":100.0},{"length":100.0},{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":200,"angle":22.5,"switch":true},{"length":100.0},{"length":100.0},{"radius":200,"angle":-22.5},{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":200,"angle":22.5},{"radius":200,"angle":-22.5},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"length":62.0},{"radius":100,"angle":-45.0,"switch":true},{"radius":100,"angle":-45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"length":100.0,"switch":true},{"length":100.0},{"length":100.0},{"length":100.0},{"length":90.0}],"lanes":[{"distanceFromCenter":-10,"index":0},{"distanceFromCenter":10,"index":1}],"startingPoint":{"position":{"x":-300.0,"y":-44.0},"angle":90.0}},"cars":[{"id":{"name":"Devs On Vacation","color":"red"},"dimensions":{"length":40.0,"width":20.0,"guideFlagPosition":10.0}}],"raceSession":{"laps":3,"maxLapTimeMs":60000,"quickRace":true}}}')
      @track = Tracks::Parser.from_data(data)
      @track.prepare_for_race(create_race)
    end

    it 'should have lanes' do
      expect(@track.lanes.size).to eq(2)
    end

    it 'should have correct sections' do
      expect(@track.sections_count).to eq(40)
    end

    it 'should have correct length' do
      expect(@track.lanes[0].sections[0].lane_distance_to_finish).to be_within(1).of(3499)
      expect(@track.lanes[1].sections[0].lane_distance_to_finish).to be_within(1).of(3374)
    end

    it 'should set switches on straight' do
      expect(@track.lanes.sample.sections[3]).to be_switchable
      expect(@track.lanes.sample.sections.count { |section| section.switchable? }).to eq(7)
    end
  end
end