require 'spec_helper'

describe Tracks::Analyzer do
  before do
    @track = create_track(3)
    @track.add_straight(100)
    @track.add_straight(200)
    @track.add_curve(200, 45)
    @track.add_curve(100, 45)
    @track.set_switch(1)
    @analyzer = Tracks::Analyzer.new(@track)
  end

  describe 'lane 0 section 0' do
    before do
      @lane, @section = 0, 0
    end

    it 'should return correct distance to finish' do
      expect(@analyzer.lane_distance_to_finish(@lane, @section)).to be_within(1).of(535)
    end

    it 'should return correct distance to turn' do
      expect(@analyzer.distance_to_curve(@lane, @section)).to eq(300)
    end
  end

  describe 'lane 1 section 2' do
    before do
      @lane, @section = 1, 2
    end

    it 'should return correct length to finish' do
      expect(@analyzer.lane_distance_to_finish(@lane, @section)).to be_within(1).of(204)
    end

    it 'should return correct distance to turn' do
      expect(@analyzer.distance_to_curve(@lane, @section)).to eq(0)
    end
  end

  describe 'lane 2 section 1' do
    before do
      @lane, @section = 2, 1
    end

    it 'should return correct length to finish' do
      expect(@analyzer.lane_distance_to_finish(@lane, @section)).to be_within(1).of(372)
    end

    it 'should return correct distance to turn' do
      expect(@analyzer.distance_to_curve(@lane, @section)).to eq(200)
    end
  end

  describe 'distance between' do
    before do
      @start = Metrics::Position.new(1, 1, 14.5)
      @end = Metrics::Position.new(1, 1, 20)
    end

    it 'should return in piece distance if start nil' do
      expect(@analyzer.distance_between(@end, nil)).to eq(20)
    end

    it 'should return in delta if same section' do
      expect(@analyzer.distance_between(@end, @start)).to eq(5.5)
    end

    it 'should return in delta if different lanes' do
      @start.lane = 2
      expect(@analyzer.distance_between(@end, @start)).to eq(5.5)
    end

    it 'should calculate distance at the end of previous lane' do
      @start.section = 0
      expect(@analyzer.distance_between(@end, @start)).to eq(105.5)
    end
  end

  describe 'distance from start' do
    before do
      @track.add_curve(200, 180)
    end

    it 'should have correct distance for first section' do
      expect(@analyzer.lane_distance_from_start(0, 0)).to eq(0)
    end

    it 'should have correct distance for second section' do
      expect(@analyzer.lane_distance_from_start(1, 1)).to eq(100)
    end

    it 'should have correct distance for third section' do
      expect(@analyzer.lane_distance_from_start(0, 3)).to be_within(1).of(457)
      expect(@analyzer.lane_distance_from_start(1, 3)).to be_within(1).of(441)
    end
  end

  describe 'best values analytic' do
    before do
      @analyzer.prepare_for_race(create_race)
    end

    it 'should have correct best distance for all lanes in section 0' do
      expect(@analyzer.short_distance_to_finish(0, 0)).to be_within(1).of(504)
      expect(@analyzer.short_distance_to_finish(1, 0)).to be_within(1).of(472)
      expect(@analyzer.short_distance_to_finish(2, 0)).to be_within(1).of(472)
    end

    it 'should have correct switches for all lanes in section 0' do
      expect(@analyzer.optimal_switches(0, 0)).to eq(nil)
      expect(@analyzer.optimal_switches(1, 0)).to eq(nil)
      expect(@analyzer.optimal_switches(2, 0)).to eq(nil)
    end

    it 'should have correct switches for all lanes in section 1' do
      expect(@analyzer.optimal_switches(0, 1)).to eq([1, 0])
      expect(@analyzer.optimal_switches(1, 1)).to eq([2, 1, 0])
      expect(@analyzer.optimal_switches(2, 1)).to eq([2, 1])
    end

    it 'should have correct switches for all lanes in section 2' do
      expect(@analyzer.optimal_switches(0, 2)).to eq(nil)
      expect(@analyzer.optimal_switches(1, 2)).to eq(nil)
      expect(@analyzer.optimal_switches(2, 2)).to eq(nil)
    end
  end

  describe 'more complex track' do
    before do
      @track = create_track(3)
      @track.add_straight(100)
      @track.add_straight(200)
      @track.set_switch(1)
      @track.add_curve(200, 180)
      @track.add_straight(400)
      @track.add_curve(200, 90)
      @track.add_straight(400)
      @track.add_curve(200, 180)
      @track.add_straight(50)
      @track.set_switch(7)
      @track.add_straight(50)
      @track.set_switch(8)
      @track.add_curve(200, -90)
      @analyzer = Tracks::Analyzer.new(@track)
      @analyzer.prepare_for_race(create_race)
    end

    it 'should have correct best distance for all lanes in section 0' do
      expect(@analyzer.short_distance_to_finish(0, 0)).to be_within(1).of(2927)
      expect(@analyzer.short_distance_to_finish(1, 0)).to be_within(1).of(2770)
      expect(@analyzer.short_distance_to_finish(2, 0)).to be_within(1).of(2770)
    end

    it 'should have correct switches for all lanes in section 1' do
      expect(@analyzer.optimal_switches(0, 1)).to eq([1, 0])
      expect(@analyzer.optimal_switches(1, 1)).to eq([2, 1, 0])
      expect(@analyzer.optimal_switches(2, 1)).to eq([2, 1])
    end

    it 'should have correct switches for all lanes in section 7' do
      expect(@analyzer.optimal_switches(0, 7)).to eq([0, 1])
      expect(@analyzer.optimal_switches(1, 7)).to eq([1, 0, 2])
      expect(@analyzer.optimal_switches(2, 7)).to eq([1, 2])
    end

    it 'should have correct switches for all lanes in section 8' do
      pending('unoptimal solution for tricky tracks') do
        expect(@analyzer.optimal_switches(0, 8)).to eq(nil)
        expect(@analyzer.optimal_switches(1, 8)).to eq(nil)
        expect(@analyzer.optimal_switches(2, 8)).to eq(1)
      end
    end
  end
end