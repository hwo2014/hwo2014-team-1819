require 'spec_helper'

describe Configurations::TeamServer do
  before do
    @config = Configurations::TeamServer.new
  end

  it 'should return correct bot key' do
    expect(@config.bot_key).to eq('mpxz2tee+ffzaQ')
  end

  it 'should return name shorter then 16 chars' do
    expect(@config.bot_name.length).to be <= 16
  end

  it 'should return correct server port' do
    expect(@config.server_port).to eq('8091')
  end

  it 'should return correct join data' do
    expect(@config.join_data[:botId]).to include(key: 'mpxz2tee+ffzaQ')
  end
end