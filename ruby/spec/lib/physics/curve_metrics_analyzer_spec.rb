require 'spec_helper'


describe Physics::CurveMetricsAnalyzer do
  before do
    @analyzer = Physics::CurveMetricsAnalyzer.new(Physics::Coefficients.new)
  end

  def metrics(angle, angle_speed, angle_acceleration)
    momentum = Metrics::Momentum.new(0, 0, 0, angle_speed, angle_acceleration)
    position = Metrics::Position.new(0, 0, 0, angle)
    Metrics::Drive.new(momentum, position)
  end

  describe 'crash danger' do
    it 'should report false when 0 angles' do
      expect(@analyzer.crash_danger?(metrics(0, 0, 0), 10, 10)).to eq(false)
    end

    it 'should report true when highest peak exceeds danger' do
      expect(@analyzer.crash_danger?(metrics(50, 2, -0.2), 10, 10)).to eq(true)
    end

    it 'should report false when highest peak inside danger' do
      expect(@analyzer.crash_danger?(metrics(-40, -2, 0.5), 10, 10)).to eq(false)
    end

    it 'should report false when highest peak outside curve' do
      expect(@analyzer.crash_danger?(metrics(40, 2, -0.2), 5, 10)).to eq(false)
    end

    it 'should report true when speed outside curve going to kick your ass' do
      expect(@analyzer.crash_danger?(metrics(40, 4, 0.2), 2, 10)).to eq(true)
    end

    it 'should support negative angles for ass kicking' do
      expect(@analyzer.crash_danger?(metrics(-40, -4, -0.2), 2, 10)).to eq(true)
    end
  end

  describe 'angular acceleration peak' do
    def acceleration_peak?(speed, acceleration)
      @analyzer.angular_acceleration_peak?(@curve, metrics(@angle, speed, acceleration))
    end

    describe 'no crash danger' do
      before do
        @speed = 2
        @acceleration = 0.2
      end

      describe 'positive curve' do
        before do
          @curve = 45
        end

        describe 'positive angle' do
          before do
            @angle = 20
          end

          it 'should report true when velocity positive and acceleration negative' do
            expect(acceleration_peak?(@speed, -@acceleration)).to eq(true)
          end

          it 'should report true when velocity negative and acceleration negative' do
            expect(acceleration_peak?(-@speed, -@acceleration)).to eq(true)
          end

          it 'should report false when velocity positive and acceleration positive' do
            expect(acceleration_peak?(@speed, @acceleration)).to eq(false)
          end

          it 'should report false when velocity negative and acceleration positive' do
            expect(acceleration_peak?(-@speed, @acceleration)).to eq(false)
          end
        end

        describe 'negative angle' do
          before do
            @angle = -20
          end

          it 'should report false when velocity positive and acceleration negative' do
            expect(acceleration_peak?(@speed, -@acceleration)).to eq(false)
          end

          it 'should report false when velocity negative and acceleration negative' do
            expect(acceleration_peak?(-@speed, -@acceleration)).to eq(false)
          end

          it 'should report false when velocity positive and acceleration positive' do
            expect(acceleration_peak?(@speed, @acceleration)).to eq(false)
          end

          it 'should report false when velocity negative and acceleration positive' do
            expect(acceleration_peak?(-@speed, @acceleration)).to eq(false)
          end
        end
      end

      describe 'negative curve' do
        before do
          @curve = -90
        end

        describe 'negative angle' do
          before do
            @angle = -20
          end

          it 'should report false when velocity positive and acceleration negative' do
            expect(acceleration_peak?(@speed, -@acceleration)).to eq(false)
          end

          it 'should report false when velocity negative and acceleration negative' do
            expect(acceleration_peak?(-@speed, -@acceleration)).to eq(false)
          end

          it 'should report true when velocity positive and acceleration positive' do
            expect(acceleration_peak?(@speed, @acceleration)).to eq(true)
          end

          it 'should report true when velocity negative and acceleration positive' do
            expect(acceleration_peak?(-@speed, @acceleration)).to eq(true)
          end
        end

        describe 'positive angle' do
          before do
            @angle = 20
          end

          it 'should report false when velocity positive and acceleration negative' do
            expect(acceleration_peak?(@speed, -@acceleration)).to eq(false)
          end

          it 'should report false when velocity negative and acceleration negative' do
            expect(acceleration_peak?(-@speed, -@acceleration)).to eq(false)
          end

          it 'should report false when velocity positive and acceleration positive' do
            expect(acceleration_peak?(@speed, @acceleration)).to eq(false)
          end

          it 'should report false when velocity negative and acceleration positive' do
            expect(acceleration_peak?(-@speed, @acceleration)).to eq(false)
          end
        end
      end
    end


  end
end