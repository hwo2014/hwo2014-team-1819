require 'spec_helper'

describe Physics::CurveSpeedCalculator do
  include Math

  before do
    @coefficients = Physics::Coefficients.new
    @calculator = Physics::CurveSpeedCalculator.new(@coefficients)
  end

  describe 'max curve speed' do
    it 'should return correct speed for 90 radius' do
      expect(@calculator.curve_max_speed(90)).to be_within(0.01).of(7.36)
    end

    it 'should return correct speed for 110 radius' do
      expect(@calculator.curve_max_speed(110)).to be_within(0.01).of(8.13)
    end

    it 'should return correct speed for 190 radius' do
      expect(@calculator.curve_max_speed(190)).to be_within(0.01).of(10.69)
    end

    it 'should return correct speed for 210 radius' do
      expect(@calculator.curve_max_speed(210)).to be_within(0.01).of(11.24)
    end
  end

  describe 'long curves right' do
    describe 'radius 90, angle 180' do
      before do
        @radius = 90
        @angle = 180
      end

      it 'should return correct point for curve with radius 90 ' do
        expect(@calculator.minimal_curve_point(@radius, @angle)).to match_array([be_within(1).of(98), be_within(0.1).of(5.34)])
      end
    end
  end
end