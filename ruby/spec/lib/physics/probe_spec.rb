require 'spec_helper'

describe Physics::Probe do

  before do
    @metrics = Metrics::Race.new
    @metrics.metrics << Metrics::Drive.new(Metrics::Momentum.new(0, 6, 1, 5, 5), Metrics::Position.new(nil, nil, nil, 15), Metrics::Distance.new(19), 7)
    @metrics.metrics << Metrics::Drive.new(Metrics::Momentum.empty, Metrics::Position.new(nil, nil, nil, 22), Metrics::Distance.new(27), 8)

    @probe =  Physics::Probe.new(@metrics)
  end

  it 'should update speed' do
    expect{ @probe.update_momentum }.to change(@metrics.last.momentum, :speed).to(8)
  end

  it 'should update acceleration' do
    expect{ @probe.update_momentum }.to change(@metrics.last.momentum, :acceleration).to(2)
  end

  it 'should update angular speed' do
    expect{ @probe.update_momentum }.to change(@metrics.last.momentum, :angular_speed).to(7)
  end

  it 'should update angular acceleration' do
    expect{ @probe.update_momentum }.to change(@metrics.last.momentum, :angular_acceleration).to(2)
  end

  describe 'Negative values ' do
    before do
      @metrics.metrics << Metrics::Drive.new(Metrics::Momentum.new(0, 8, 0, 4, 5), Metrics::Position.new(nil, nil, nil, -16), Metrics::Distance.new(35), 7)
      @metrics.metrics << Metrics::Drive.new(Metrics::Momentum.empty, Metrics::Position.new(nil, nil, nil, -14), Metrics::Distance.new(41), 8)
    end

    it 'should update speed' do
      expect{ @probe.update_momentum }.to change(@metrics.last.momentum, :speed).to(6)
    end

    it 'should update acceleration' do
      expect{ @probe.update_momentum }.to change(@metrics.last.momentum, :acceleration).to(-2)
    end

    it 'should update angular speed' do
      expect{ @probe.update_momentum }.to change(@metrics.last.momentum, :angular_speed).to(2)
    end

    it 'should update angular acceleration' do
      expect{ @probe.update_momentum }.to change(@metrics.last.momentum, :angular_acceleration).to(-2)
    end
  end

  describe 'switching lanes spikes' do
    describe 'after switched' do
      before do
        @metrics.previous.position.start_lane = 0
      end

      it 'should use old speed' do
        expect { @probe.update_momentum }.to change(@metrics.last.momentum, :speed).to(6)
      end

      it 'should use old acceleration' do
        expect { @probe.update_momentum }.to change(@metrics.last.momentum, :acceleration).to(1)
      end

      it 'should update angular speed' do
        expect { @probe.update_momentum }.to change(@metrics.last.momentum, :angular_speed).to(7)
      end

      it 'should update angular acceleration' do
        expect { @probe.update_momentum }.to change(@metrics.last.momentum, :angular_acceleration).to(2)
      end
    end

    describe 'during switch' do
      before do
        @metrics.previous.position.start_lane = 0
        @metrics.last.position.start_lane = 0
      end

      it 'should update speed' do
        expect{ @probe.update_momentum }.to change(@metrics.last.momentum, :speed).to(8)
      end

      it 'should update acceleration' do
        expect{ @probe.update_momentum }.to change(@metrics.last.momentum, :acceleration).to(2)
      end

      it 'should update angular speed' do
        expect { @probe.update_momentum }.to change(@metrics.last.momentum, :angular_speed).to(7)
      end

      it 'should update angular acceleration' do
        expect { @probe.update_momentum }.to change(@metrics.last.momentum, :angular_acceleration).to(2)
      end
    end

  end
end
