require 'spec_helper'

describe Physics::StraightThrottleControl do
  before do
    @physics = Physics::Coefficients.new
    @control = Physics::StraightThrottleControl.new(@physics)
    @friction = @physics.friction
    @acceleration = @physics.power
  end

  describe 'target velocity greater then now' do
    it 'should return correct throttle for large distance' do
      expect(@control.get_throttle(1, 9.5, 1000)).to eq(1.0)
    end

    it 'should return correct throttle for small distance' do
      expect(@control.get_throttle(1, 9.5, 0.5)).to eq(0)
    end
  end

  describe 'target velocity same as current' do
    it 'should return accelerate while not reached breaking point' do
      expect(@control.get_throttle(6.5, 6.5, 1000)).to eq(1.0)
    end

    it 'should return correct throttle for small distance' do
      expect(@control.get_throttle(6.5, 6.5, 0.5)).to eq(0)
    end
  end

  describe 'target velocity less then' do
    it 'should return correct throttle for large distance' do
      expect(@control.get_throttle(9.5, 1, 1000)).to eq(1.0)
    end

    it 'should return correct throttle for distance before to breaking point' do
      expect(@control.get_throttle(9.5, 1, (8.5 / @friction) + 1.0001)).to eq(1)
    end

    it 'should return correct throttle for distance after breaking point' do
      expect(@control.get_throttle(9.5, 1, (8.5 / @friction) + 0.9)).to eq(0)
    end
  end
end