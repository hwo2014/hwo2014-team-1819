require 'spec_helper'
require 'csv'

describe Mathematics::LinearFit do

  def data2
    data = []
    CSV.foreach("spec/lib/mathematics/data2.csv") do |row|
      row.map!{ |el| el.to_f }
      data << row
    end
    data
  end

  def data3
    data = []
    CSV.foreach("spec/lib/mathematics/data3.csv") do |row|
      row.map!{ |el| el.to_f }
      data << row
    end
    data
  end

  describe 'check on data2' do
    before do
      num_cols = data2.first.size
      x, y = [], []
      data2.each do |row|
        x << row[0..(num_cols - 2)]
        y << row.last
      end

      @solver = Mathematics::LinearFit.new(x, y)
    end

    it 'should get theta matrix' do
      theta = @solver.theta.to_a.flatten
      theta.first.should be_within(0.001).of(-3.8958)
      theta.last.should be_within(0.001).of(1.1930)
    end

    it 'should calculate' do
      res = -3.8958 + 1.1930 * 10
      @solver.calculate([10]).should be_within(0.1).of(res)
    end

  end


  describe 'check on data3' do
    before do
      num_cols = data3.first.size
      x, y = [], []
      data3.each do |row|
        x << row[0..(num_cols - 2)]
        y << row.last
      end
      @solver = Mathematics::LinearFit.new(x, y)
    end

    it 'should get theta matrix' do
      theta = @solver.theta.to_a.flatten
      theta[0].should be_within(1).of(8.9598e+04)
      theta[1].should be_within(0.1).of(1.3921e+02)
      theta[2].should be_within(0.1).of(-8.7380e+03)
    end

    it 'should calculate' do
      res = 8.9598e+04 + 1.3921e+02 * 5 + -8.7380e+03 * 10
      @solver.calculate([5, 10]).should be_within(1).of(res)
    end
  end


end

