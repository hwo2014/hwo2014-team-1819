require 'spec_helper'

describe Mathematics::DiffSolver do
  before do
    @physics = Physics::Coefficients.new
    @solver = Mathematics::DiffSolver.new(@physics)
  end

  def metric(speed, angle, angle_speed)
    metric = Metrics::Drive.empty
    metric.position.angle = angle
    metric.momentum.speed = speed
    metric.momentum.angular_speed = angle_speed
    metric
  end
  describe 'Liner' do

    describe 'acceleration' do
      before do
        @solver.starting_at(metric(0, 0, 0))
        @solver.throttle = 1
      end

      it 'should increase x and v' do
        @solver.calculate_tick
        expect(@solver.x > 0).to be_true
        expect(@solver.lin_speed > 0).to be_true
      end

      it 'should reach limit' do
        200.times { @solver.calculate_tick }
        expect(@solver.lin_speed).to be_within(0.5).of(10)
      end

    end

    describe 'deceleration' do
      before do
        @solver.starting_at(metric(10, 0, 0))
        @solver.throttle = 0
      end

      it 'should increase x and decrease v' do
        @solver.calculate_tick
        expect(@solver.x > 0).to be_true
        expect(@solver.lin_speed < 10).to be_true
      end

      it 'should reach limit' do
        200.times { @solver.calculate_tick }
        expect(@solver.lin_speed).to be_within(0.5).of(0)
      end

    end

  end


  describe 'Angular on the straight' do
    before do
      @solver.starting_at(metric(5, 30, -3))
      @solver.throttle = 1
    end

    it 'should decrease angular speed' do
      @solver.calculate_tick
      expect(@solver.ang_speed).to be < -2.8
      expect(@solver.a).to be < 28
    end

    it 'should reach limit' do
      100.times {
        @solver.calculate_tick
      }
      expect(@solver.a).to be_within(0.5).of(0)
    end
  end


  describe 'get_solver_after_distance' do
    before do
      @solver.starting_at(metric(5, 30, 3))
      @solver.throttle = 1
    end

    it 'should get_solver_after_distance' do
      solver = @solver.get_solver_after_distance(1000)
      expect(solver.lin_speed > 5).to be_true
      expect(@solver.a).to be_within(0.5).of(0)
    end

  end

  describe 'will_crash_on_distance' do

    it 'should not crash' do
      @solver.starting_at(metric(5, 30, 3))
      expect(@solver.will_crash_on_distance(100)).to be_false
    end

    it 'should crash' do
      @solver.starting_at(metric(5, -30, 15))
      expect(@solver.will_crash_on_distance(100)).to be_true
    end
  end
end
