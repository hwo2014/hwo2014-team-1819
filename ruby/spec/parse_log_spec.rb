require 'spec_helper'
require 'yaml'

describe 'Parse log' do
  before do
    @yaml = YAML.load(File.read('log/metrics.yml'))
  end

  it 'should ' do
    positions = @yaml.map{|el| el[:distance][:absolute]}
    angles = @yaml.map{|el| el[:position][:angle]}
    throttles = @yaml.map{|el| el[:momentum][:throttle]}
    distances = []
    @alphas = []
    @throttles = []
    positions.each_with_index do |distance, i|
      distances << distance
      @alphas << angles[i]
      @throttles << throttles[i]
    end
    calculate(distances)
    calculate_al

    save_to_file('tmp/metrics.txt')
  end

  def calculate(xx)
    @xx = xx
    @vv = []
    @xx.each_with_index do |xx, i|
      unless i == 0
        @vv << xx - @xx[i - 1]
      end
    end

    @aa = []
    @vv.each_with_index do |vv, i|
      unless i == 0
        @aa << vv - @vv[i - 1]
      end
    end

  end

  def calculate_al
    @alpha_v = []
    @alphas.each_with_index do |xx, i|
      unless i == 0
        @alpha_v << xx - @alphas[i - 1]
      end
    end

    @alpha_a = []
    @alpha_v.each_with_index do |vv, i|
      unless i == 0
        @alpha_a << vv - @alpha_v[i - 1]
      end
    end
  end

  def save_to_file(file)
    File.open(file, 'w') do |file|
      @xx.size.times do |i|
        file.puts("#{i},#{@xx[i] || 0},#{@vv[i] || 0},#{@aa[i] || 0},#{@alphas[i] || 0},#{@alpha_v[i] || 0},#{@alpha_a[i] || 0},#{@throttles[i] || 0}")
      end
    end
  end
end
