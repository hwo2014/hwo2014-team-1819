require 'json'
require 'socket'
require 'forwardable'
require 'pry'

# Require lib files
Dir[File.expand_path(File.join(File.dirname(__FILE__), '../lib', '**', '*.rb'))].each { |f| require f }
# Requires supporting files with custom matchers and macros, etc,
Dir[File.expand_path(File.join(File.dirname(__FILE__), 'support', '**', '*.rb'))].each { |f| require f }

RSpec.configure do |config|
end
