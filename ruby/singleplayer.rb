require 'json'
require 'socket'
require 'forwardable'
require 'pry'

Dir[File.expand_path(File.join(File.dirname(__FILE__), 'lib', '**', '*.rb'))].each { |f| require f }

race = Games::Tournament.new(Drivers::Lauda, Configurations::TeamServer.new)
race.start('france', 1, true)
